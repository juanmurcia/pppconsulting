var elixir = require('laravel-elixir');

elixir(function (mix) {
    mix.styles([
        "/vendors/bootstrap/dist/css/bootstrap.min.css",
        "/vendors/font-awesome/css/font-awesome.min.css",
        "/vendors/nprogress/nprogress.min.css",
        "/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css",
        "/vendors/bootstrap-daterangepicker/daterangepicker.min.css",
        "/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.css",
        "/vendors/toastr/toastr.min.css",
        "/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
        "/build/css/custom.min.css",
        "/vendors/select2/dist/css/select2.min.css",
        "/flag/flags.min.css",
    ], "public/css/consulting.css", "public")
        .styles([
            "/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css",
            "/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css",
            "/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css",
            "/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css",
            "/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css",
        ], "public/css/datatables.css", "public")
        .scripts([
            "/vendors/jquery/dist/jquery.min.js",
            "/vendors/bootstrap/dist/js/bootstrap.min.js",
            "/vendors/fastclick/lib/fastclick.js",
            "/vendors/nprogress/nprogress.js",
            "/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js",
            "vendors/moment/min/moment.min.js",
            "vendors/bootstrap-daterangepicker/daterangepicker.js",
            "vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
            "vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js",
            "vendors/parsleyjs/dist/parsley.min.js",
            "vendors/bootstrap-progressbar/bootstrap-progressbar.min.js",
            "vendors/select2/dist/js/select2.min.js",
            "build/js/custom.min.js",
            "vendors/toastr/toastr.min.js",
            "vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js",
            "vendors/jquery.tagsinput/src/jquery.tagsinput.js",
            "js/funciones.js",
            "js/audit/table.js",
        ], "public/js/consulting.js", "public")
        .scripts([
            "vendors/datatables.net/js/jquery.dataTables.min.js",
            "vendors/datatables.net-bs/js/dataTables.bootstrap.min.js",
            "vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js",
            "vendors/datatables.net-keytable/js/dataTables.keyTable.min.js",
            "vendors/datatables.net-responsive/js/dataTables.responsive.min.js",
            "vendors/datatables.net-scroller/js/dataTables.scroller.min.js",
            "vendors/datatables.net-buttons/js/dataTables.buttons.js",
            "vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js",
            "vendors/datatables.net-buttons/js/buttons.flash.min.js",
            "vendors/datatables.net-buttons/js/buttons.html5.js",
            "vendors/datatables.net-buttons/js/buttons.print.min.js",
            "vendors/datatables.net-buttons/js/jszip.min.js",
            "vendors/datatables.net-buttons/js/pdfmake.min.js",
            "vendors/datatables.net-buttons/js/vfs_fonts.js",
            "vendors/datatables/datatable.js",
        ], "public/js/datatables.js", "public");
});