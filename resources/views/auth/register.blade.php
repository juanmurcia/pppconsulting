@extends('layouts.empty')

@section('content')
    <div class="page-login" >
        <div class="row" >
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4" >
                <div class="logo-login text-center">
                    <img src="{{ asset('/images/logo.svg') }}" style="margin:2em 0; width: 60%">
                </div>
                <div class="box-login">
                    <form class="form-horizontal form-label-left" id="frmRegister" name="frmRegister" onsubmit="return false;">
                        {{ csrf_field() }}
                        <div class="row">
                            <br><br><br>
                            <div class="col-xs-10 col-xs-offset-1">
                                @include('user.new')
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="col-xs-8 col-xs-offset-2 text-center">
                            <button type="submit" id="actionRegister" data-role="add" class="btn btn-lg btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i><span class="hidden-xs"> {{ trans_choice('text.register',1) }}</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{asset('css/login.css')}}" rel="stylesheet">
@endsection