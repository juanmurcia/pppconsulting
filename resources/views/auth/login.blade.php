@extends('layouts.empty')

@section('content')
    <div class="page-login" >
        <div class="row" >
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4" >
                <div class="logo-login text-center">
                    <img src="{{ asset('/images/logo.svg') }}" style="margin:2em 0; width: 60%">
                </div>
                <div class="box-login">
                    <form data-parsley-validate style="margin-top: 0%" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <br><br><br>
                            <div class="col-xs-10 col-xs-offset-1">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger alert-dismissible">
                                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                                        <i class="icon fa fa-ban"></i><strong>Whoops!</strong> Hay algunos problemas en la autenticacion.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                                </div>
                                <div class="form-group">
                                    <input id="password" placeholder="Password" type="password" class="form-control" name="password" required>
                                </div>
                                <br>
                                <div class="social-auth-links text-center">
                                    <a href="#"><p>{{ trans_choice('text.forgot_password',1) }}</p></a>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
                                        <button type="submit" style="color: white;" class="btn btn-block btn-primary btn-lg">
                                            {{ trans_choice('text.log_in',1) }}
                                        </button>
                                    </div>
                                </div>
                                <div class="social-auth-links text-center">
                                    <span><strong>{{ trans_choice('text.not_member',1) }} </strong><a href="/account">{{ trans_choice('text.create_account',1) }}</a></span>
                                </div>
                                <br>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{asset('css/login.css')}}" rel="stylesheet">
@endsection