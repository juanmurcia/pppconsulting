<div class="modal fade" id="modAudit" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" id="titleModalUser">{{ trans_choice('text.audit_model',1) }}</h3>
                <div class="clearfix"></div>
            </div>

            <div class="modal-body" style="overflow: auto;white-space: nowrap;">
                <table id="tbAudit" class="table table-striped table-hover" style="width: 100%;">
                    <thead>
                        <tr class="completeAudit">
                            <th>{{ trans_choice('text.created_at',1) }}</th>
                            <th>{{ trans_choice('text.user_cre',1) }}</th>
                            <th>{{ trans_choice('text.state',1) }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>