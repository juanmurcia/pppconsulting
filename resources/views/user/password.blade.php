<div id="profilePass" class="profile" style="display: none;">
    <div class="row">
        <h1>{{ trans_choice('text.reset_password',1,[],Auth()->user()->lang) }}</h1>
        <br>
    </div>
    <form action="#" data-parsley-validate class="form-horizontal" id="frmPassword" name="frmPassword" onsubmit="return false;">
        <div class="row">
            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label">{{ trans_choice('text.current',1,[],Auth()->user()->lang) }} {{ trans_choice('text.password',1,[],Auth()->user()->lang) }} <span class="required">*</span></label>
                    <input required type="password" id="txtPassPrev" name="txtPassPrev" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label" >{{ trans_choice('text.new',1,[],Auth()->user()->lang) }} {{ trans_choice('text.password',1,[],Auth()->user()->lang) }} </label>
                    <input required type="password" id="txtPassUserNew" name="txtPassUserNew" class="form-control">
                </div>
                <div class="col-md-6">
                    <label class="control-label" >{{ trans_choice('text.confirm',1,[],Auth()->user()->lang) }} {{ trans_choice('text.password',1,[],Auth()->user()->lang) }} <span class="required">*</span></label>
                    <input required type="password" id="txtPassUserConf" name="txtPassUserConf" class="form-control">
                </div>
            </div>
        </div>
        <div class="ln_solid"></div>
        <div class="col-xs-8 col-sm-4 col-sm-offset-6 col-md-4 col-md-offset-8">
            <button type="submit" id="actionPassword" data-role="add" class="btn btn-lg btn-block btn-success">
                {{ trans_choice('text.save',1,[],Auth()->user()->lang) }}
            </button>
        </div>
        {{ csrf_field() }}
    </form>
</div>