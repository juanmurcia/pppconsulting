@extends('layouts.empty')

@section('content')
    <div class="row" style="margin-top: 8%; margin-bottom: 3%">
        <div class="col-md-6 col-md-offset-3">
            <div class="x_panel text-center" style="margin-left: 3%;">
                <div class="page-title text-center" style="margin-bottom: 3%">
                    <h1>
                        <span class="glyphicon glyphicon-ok-circle"></span><br>
                        {{ trans_choice('text.finished',2,[]) }}
                    </h1>
                </div>

                <br>
                <h4 class="text-center">{{ trans_choice('text.verify_email',2,[]) }}</h4>
                <div class="col-xs-8 col-sm-3 col-sm-offset-6 col-md-4 col-md-offset-4">
                    <a href="{{ route('home') }}" class="btn btn-lg btn-block btn-primary">
                        {{ trans_choice('text.continue',1,[]) }}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{asset('css/login.css')}}" rel="stylesheet">
@endsection