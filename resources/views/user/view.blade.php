@extends('layouts.app')

@section('content')
    <div class="row" style="margin-top: 8%; margin-bottom: 3%">
        <div class="col-md-3 menu_fixed" style="margin-left: 3%">
            <div class="x_panel">
                <div class="row">
                    <ul class="messages hover">
                        <li>
                            <blockquote style="cursor: pointer;" id="ty1" class="message" onclick="form_profile(1);">{{ trans_choice('text.edit_profile',1,[],Auth()->user()->lang) }}</blockquote>
                            <blockquote style="cursor: pointer;" id="ty2" class="message" onclick="form_profile(2);">{{ trans_choice('text.reset_password',1,[],Auth()->user()->lang) }}</blockquote>
                            <blockquote class="message">{{ trans_choice('text.billing_information',1,[],Auth()->user()->lang) }}</blockquote>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-8" >
            <div class="x_panel" style="margin-left: 3%;">
                <div class="page-title" style="margin-bottom: 3%">
                    <div class="row">
                        <div class="col-md-12">
                            @include('user.profile')
                            @include('user.password')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @parent
    <script src="{{asset('js/user/profile.js')}}"></script>
@endsection