@extends('layouts.app')

@section('page_name') {{ trans_choice('text.user',2) }} @stop

@section('content')
    <div class="x_panel" style="margin-top: 8em">
        <div style="float: right">
            @can('user.create')
                <button type="button" id="btnUser" class="btn btn-lg btn-primary"
                        onclick="createUser()" data-toggle="modal" data-target="#modUser">
                    <i class="fa fa-plus" aria-hidden="true"></i><span
                            class="hidden-xs"> {{ trans_choice('text.create',1) }} {{ trans_choice('text.user',1) }}</span>
                </button>
            @endcan()
            <button type="button" style="float: right" class="btn btn-lg btn-default" data-toggle="modal"
                    data-target="#modSelectColumn">
                <i class="fa fa-sliders" aria-hidden="true"></i> <span
                        class="hidden-xs"> {{ trans_choice('text.column',2) }}</span>
            </button>
        </div>

        <table id="tbUser" class="table table-striped table-hover col-12">
            <thead>
            <tr>
                <th>Role</th>
                <th>{{ trans_choice('text.name',1) }}</th>
                <th>{{ trans_choice('text.last_name',1) }}</th>
                <th>{{ trans_choice('text.email',1) }}</th>
                <th>{{ trans_choice('text.phone',1) }}</th>
                <th>{{ trans_choice('text.action',1) }}</th>
            </tr>
            </thead>
        </table>
    </div>
@stop

@section('modals')
    @parent
    <div class="modal fade" id="modUser" role="dialog" style="margin-top: 8em">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">{{ trans_choice('text.manage',1) }} {{ trans_choice('text.user',1) }}</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <form action="#" data-parsley-validate class="form-horizontal" id="frmUser" name="frmUser" onsubmit="return false;">
                        @include('user.new')
                        <div class="ln_solid"></div>
                        <div class="col-xs-12 text-center">
                            <button type="submit" id="actionUser" data-role="add" class="btn btn-lg btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i><span class="hidden-xs"> {{ trans_choice('text.save',1) }}</span>
                            </button>
                            <button type="button" data-dismiss="modal" class="btn btn-lg btn-default">
                                <i class="fa fa-close" aria-hidden="true"></i><span class="hidden-xs"> {{ trans_choice('text.cancel',1) }}</span>
                            </button>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modContract" role="dialog" style="margin-top: 8em">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">{{ trans_choice('text.manage',1) }} {{ trans_choice('text.user',1) }}</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <form action="#" data-parsley-validate class="form-horizontal" id="frmContract" name="frmContract" onsubmit="return false;">
                        <div class="form-group col-xs-12">
                            <label class="control-label col-sm-3 col-xs-12" for="first-name">{{ trans_choice('text.program',1) }}</label>
                            <div class="col-sm-8 col-xs-12">
                                <select required id="optProgram" name="optProgram" class="select2_single form-control"  >
                                    <option value="">..Seleccione uno ..</option>
                                    @foreach (App\Models\Program::where("state_id",20)->get() as $key => $val)
                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 text-center">
                            <button type="submit" id="actionContract" data-role="add" class="btn btn-lg btn-success">
                                <i class="fa fa-check" aria-hidden="true"></i><span class="hidden-xs"> {{ trans_choice('text.save',1) }}</span>
                            </button>
                            <button type="button" data-dismiss="modal" class="btn btn-lg btn-default">
                                <i class="fa fa-close" aria-hidden="true"></i><span class="hidden-xs"> {{ trans_choice('text.cancel',1) }}</span>
                            </button>
                        </div>
                        {{ csrf_field() }}
                    </form>
                    <table id="tbContract" class="table table-striped table-hover col-12" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans_choice('text.name',1) }}</th>
                            <th>{{ trans_choice('text.advance',1) }}</th>
                            <th>{{ trans_choice('text.action',1) }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('style')
    @parent
    <link href="{{ asset('/css/datatables.css') }}" rel="stylesheet">
@stop


@section('script')
    @parent
    <script src="{{ asset('/js/datatables.js') }}"></script>
    <script src="{{asset('js/user/table.js')}}"></script>
@stop