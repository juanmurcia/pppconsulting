@slot('header')
@component('mail::header', ['url' => config('app.name')])
{{ config('app.name') }}
@endcomponent
@endslot
@component('mail::message')
#<h2>Hola {{ $name }}, gracias por registrarte en <strong>Viveka Consulting</strong> !</h2><br>
Por favor confirma tu correo electrónico. <br>
Para ello simplemente debes hacer click en el siguiente enlace:<br>

@component('mail::button', ['url' => env('APP_URL').'/register/verify/'.$confirmation_code ])
Verificar Correo
@endcomponent

Cordialmente,<br>
{{ config('app.name') }}
@endcomponent