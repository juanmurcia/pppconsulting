<div class="row">
	<div class="form-group col-xs-12">
		<label class="control-label col-sm-3 col-xs-12" >{{ trans_choice('text.name',1) }} </label>
		<div class="col-sm-8 col-xs-12">
			<input type="text" id="name" name="name" required class="form-control col-xs-12">
		</div>
	</div>

	<div class="form-group col-xs-12">
		<label class="control-label col-sm-3 col-xs-12" >{{ trans_choice('text.last_name',1) }} </label>
		<div class="col-sm-8 col-xs-12">
			<input type="text" id="last_name" name="last_name" required class="form-control col-xs-12">
		</div>
	</div>

	<div class="form-group col-xs-12">
		<label class="control-label col-sm-3 col-xs-12" >{{ trans_choice('text.email',1) }}</label>
		<div class="col-sm-8 col-xs-12">
			<input type="email" id="email" name="email" data-parsley-trigger="change" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" required="required" class="form-control col-xs-12">
		</div>
	</div>

	<div class="form-group col-xs-12">
		<label class="control-label col-sm-3 col-xs-12" >{{ trans_choice('text.phone',2) }}</label>
		<div class="col-sm-8 col-xs-12">
			<input type="text" id="phone" name="phone" class="tags form-control">
		</div>
	</div>

	<div id="divPasswords">
		<div class="form-group col-xs-12">
			<label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans_choice('text.password',1) }} </label>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<input type="password" id="password" name="password" required class="form-control col-xs-12">
			</div>
		</div>

		<div class="form-group col-xs-12">
			<label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans_choice('text.confirm',1) }} </label>
			<div class="col-md-8 col-sm-8 col-xs-12">
				<input type="password" id="confirm" name="confirm" required class="form-control col-xs-12">
			</div>
		</div>
	</div>
</div>

@section('script')
	@parent
	<script src="{{asset('js/user/new.js')}}"></script>
@stop
