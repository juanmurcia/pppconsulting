<div id="profile" class="profile">
    <div class="row">
        <h1>{{ trans_choice('text.edit_profile',1,[],Auth()->user()->lang) }}</h1>
        <br>
    </div>
    <form action="#" data-parsley-validate class="form-horizontal" id="frmUser" name="frmUser" onsubmit="return false;">
        <div class="row">
            <div class="form-group">
                <div class="col-md-6">
                    <label  for="name">{{ trans_choice('text.name',1,[],Auth()->user()->lang) }} <span class="required">*</span></label>
                    <input required type="text" id="name" name="name" value="{{ Auth()->user()->name }}" class="form-control">
                </div>
                <div class="col-md-6">
                    <label for="name">{{ trans_choice('text.last_name',1,[],Auth()->user()->lang) }} <span class="required">*</span></label>
                    <input type="text" id="last_name" name="last_name" value="{{ Auth()->user()->last_name }}" required class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label" for="first-name">{{ trans_choice('text.address',1,[],Auth()->user()->lang) }} </label>
                    <input required type="text" id="address" name="address" value="{{ Auth()->user()->address }}" class="form-control">
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="first-name">{{ trans_choice('text.email',1,[],Auth()->user()->lang) }} <span class="required">*</span></label>
                    <input required type="email" readonly id="email" name="email" value="{{ Auth()->user()->email }}"  pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" required="required" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label" for="first-name">{{ trans_choice('text.phone',1,[],Auth()->user()->lang) }} <span class="required">*</span></label>
                    <input required type="text" id="phone" name="phone" value="{{ Auth()->user()->phone }}" class="form-control">
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="first-name">{{ trans_choice('text.language',1,[],Auth()->user()->lang) }} <span class="required">*</span></label>
                    <div>
                        <span id="en" style="cursor: pointer"  >
                            <img src="{{ asset('flag/blank.gif') }}" onclick="select_lang(this);" class="flag flag-gb" data-lang="en" />&nbsp;
                        </span>
                        <span id="es" style="cursor: pointer"  >
                            <img src="{{ asset('flag/blank.gif') }}" onclick="select_lang(this);" class="flag flag-es" data-lang="es" />&nbsp;
                        </span>
                        <span id="fr" style="cursor: pointer">
                            <img src="{{ asset('flag/blank.gif') }}" onclick="select_lang(this);" class="flag flag-fr" data-lang="fr" />&nbsp;
                        </span>
                        <input required type="hidden" id="lang" name="lang" value="{{ Auth()->user()->lang }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="ln_solid"></div>
        <div class="col-xs-8 col-sm-4 col-sm-offset-6 col-md-4 col-md-offset-8">
            <button type="submit" id="actionUser" data-role="add" class="btn btn-lg btn-block btn-success">
                {{ trans_choice('text.save',1,[],Auth()->user()->lang) }}
            </button>
        </div>
        {{ csrf_field() }}
    </form>
</div>