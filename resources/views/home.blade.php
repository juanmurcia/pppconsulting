@extends('layouts.app')

@section('content')
    <div class="page-title row-flow " style="margin-top: 8em">
        <h1 class="text-center">{{ trans_choice('text.programs_title',1,[],Auth()->user()->lang) }}
            @can('program.create')
                <div style="float: right">
                    <button type="button" id="btnAddProgram" class="btn btn-lg btn-primary" title="Agregar nuevo Programa" onclick="new_program();">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        <span class="hidden-xs" style="margin-left: 16px">Nuevo Programa</span>
                    </button>
                </div>

                <form action="#" id="frmAddProgram" name="frmAddProgram" onsubmit="return false;">{{ csrf_field() }}</form>
            @endcan
        </h1>
    </div>

    <div class="container-fluid">
        <div class="col-md-10 col-sm-offset-1 col-xs-12">
            <div class="slick-inner" id="detailPrograms"></div>
        </div>
    </div>

    <div style="display: none;">
        <span id="continue">{{ trans_choice('text.continue_program',1,[],Auth()->user()->lang) }}</span>
        <span id="learn">{{ trans_choice('text.learn_more',1,[],Auth()->user()->lang) }}</span>
        <span id="coming">{{ trans_choice('text.coming_soon',1,[],Auth()->user()->lang) }}</span>

        <div id="frmComplete">
            <div class="row">
                <div class="form-group col-xs-12">
                    <label class="control-label col-lg-3 col-sm-3 col-xs-12">Lenguaje <span class="required">*</span></label>
                    <div class="col-sm-8 col-xs-12">
                        <div class="form-group text-left">
                            <div class="checkbox">
                                <label><input type="checkbox" name="chkLang[]" id="chkLangEsp" value="es"> Español</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="chkLang[]" id="chkLangEng" value="en"> Inglés</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" name="chkLang[]" id="chkLangFr" value="fr"> Francés</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-xs-12">
                    <label class="control-label col-lg-3 col-sm-3 col-xs-12">Estado <span class="required">*</span></label>
                    <div class="col-sm-8 col-xs-12">
                        <select class="form-control" id="optEstProgram" name="optEstProgram">
                            @foreach ( App\Models\State::where('pattern_id',2)->get() as $key => $val)
                                <option value="{{ $val->id }}">{{ $val->id }} - {{ $val->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="util" style="display: none;">
        <div id="divProgramCopy" class="item col-md-4 col-xs-12" style="margin-top: 1em;margin-bottom: 1em">
            <div class="x_logo text-center xLogo"></div>
            <div class="x_panel">
                <div class=" text-center">
                    <h2 class="text-center title"></h2>
                </div>
                <div class="x_content" style="min-height:200px;overflow-x:hidden; overflow-y:auto;">
                    <div class="row divProgress" style="display: none;">
                        <div class="progressBar col-xs-8 col-xs-offset-2 text-center">
                            <div class="progress progress_sm">
                                <div class="progressBar progress-bar bg-green" role="progressbar"></div>
                            </div>
                            <small><span class="advance"></span>% {{ trans_choice('text.complete',1,[],Auth()->user()->lang) }}</small>
                        </div>
                    </div>
                    <div class="row divBlock">
                        <div class="text-center">
                            <h3><i class="fa fa-lock bg-grey"></i></h3>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <blockquote>
                            <p class="text-muted text-center description"></p>
                        </blockquote>
                    </div>
                </div>
                <div class="row">
                    <div class="frmComplete"></div>
                    <div class="col-md-6 col-md-offset-3 col-xs-8 col-xs-offset-2 text-center language">
                        {{ trans_choice('text.language',1,[],Auth()->user()->lang) }} : <span class="langProgram"></span>
                    </div>
                    <div class="col-xs-8 col-xs-offset-2">
                        <a class="btn-vote btn btn-block btn-lg linkProgram"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @parent
    <link rel="stylesheet" type="text/css" href="/vendors/slick-1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/vendors/slick-1.8.1/slick/slick-theme.css"/>
@endsection

@section('script')
    @parent
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/vendors/slick-1.8.1/slick/slick.min.js"></script>
    <script src="{{asset('js/home.js')}}"></script>
@endsection