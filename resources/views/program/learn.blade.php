@extends('layouts.app')

@section('content')
    <input type="hidden" id="programId" value="{{ $id }}">
    <div class="row" style="margin-top: 8%; margin-bottom: 3%">
        <div class="col-md-3 menu_fixed" style="margin-left: 3%">
            <div class="x_logo text-center"></div>
            <div class="x_panel">
                <div class="col-md-12 text-center">
                    <h2>US <span class="cost"></span></h2>
                    <a id="billing" class="btn-vote btn btn-block btn-lg btn-success">{{ trans_choice('text.apply_program',1,[],Auth()->user()->lang) }}</a>
                    <div class="row">
                        @can('program.update')
                            <div class="col-md-6">
                                <button type="button" id="btnEditProgram" class="btn btn-lg btn-block btn-default" title="Editar Programa" onclick="edit_program();">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                    <span class="hidden-xs" style="margin-left: 16px">Editar</span>
                                </button>
                            </div>
                            <div class="col-md-6">
                                <button type="button" id="btnNextProgram" class="btn btn-lg btn-block btn-primary" title="Continuar Programa" onclick="next_program();">
                                    <i class="fa fa-tasks" aria-hidden="true"></i>
                                    <span class="hidden-xs" style="margin-left: 16px">Clases</span>
                                </button>
                            </div>

                            <div id="frmComplete" style="display:none;">
                                <div class="row">
                                    <div class="form-group col-xs-12">
                                        <label class="control-label col-lg-3 col-sm-3 col-xs-12">Lenguaje <span class="required">*</span></label>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="form-group text-left">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="chkLang[]" id="chkLanges" value="es"> Español</label>
                                                </div>
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="chkLang[]" id="chkLangen" value="en"> Inglés</label>
                                                </div>
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="chkLang[]" id="chkLangfr" value="fr"> Francés</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12">
                                        <label class="control-label col-lg-3 col-sm-3 col-xs-12">Estado <span class="required">*</span></label>
                                        <div class="col-sm-8 col-xs-12">
                                            <select class="form-control" id="optEstProgram" name="optEstProgram">
                                                @foreach ( App\Models\State::where('pattern_id',2)->get() as $key => $val)
                                                    <option value="{{ $val->id }}">{{ $val->id }} - {{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12">
                                        <label class="control-label col-lg-3 col-sm-3 col-xs-12">Des. Corta <span class="required">*</span></label>
                                        <div class="col-sm-8 col-xs-12">
                                            <textarea class="form-control" id="landing_description" name="landing_description"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <form style="display: none" action="#" id="frmAddProgram" name="frmAddProgram" onsubmit="return false;">
                                <input type="text" id="landing_video" name="landing_video">
                                {{ csrf_field() }}
                            </form>
                        @endcan
                    </div>
                    <div style="display: none" id="divBilling" class="row">
                        <h5 class="text-center">Forma de Pago:</h5>
                        <div class="bs-glyphicons col-md-12 text-center">
                            <a href="/program/{{ $id }}/paypal" class="btn btn-lg btn-default col-xs-5 col-xs-offset-1">
                                <span class="fa fa-paypal" aria-hidden="true"></span>
                                <span class="glyphicon-class">PayPal</span>
                            </a>
                            <a class="btn btn-lg btn-default col-xs-5 " onclick="$('#divBillingInfo').show();$('#divInfo').hide()">
                                <span class="fa fa-credit-card" aria-hidden="true"></span>
                                <span class="glyphicon-class">{{ trans_choice('text.credit_card',1,[],Auth()->user()->lang) }}</span>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-8" style="display: none" id="divBillingInfo">
            <div class="x_panel" style="margin-left: 3%;">
                <div class="page-title" style="margin-bottom: 3%">
                    <h1>{{ trans_choice('text.billing_info',1,[],Auth()->user()->lang) }}</h1>
                </div>

                <form action="#" id="frmPayProgram" name="frmPayProgram" class="form-horizontal form-label-left" onsubmit="return false;">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans_choice('text.name',1,[],Auth()->user()->lang) }} <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input required type="text" id="name" name="name" value="{{ Auth()->user()->name }}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans_choice('text.last_name',1,[],Auth()->user()->lang) }} <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input required type="text" id="last_name" name="last_name" value="{{ Auth()->user()->last_name }}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans_choice('text.card_number',1,[],Auth()->user()->lang) }} <span class="required">*</span></label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <input required type="text" id="number" name="number" class="form-control">
                        </div>
                        <label class="control-label col-md-1 col-sm-1 col-xs-12">CVV <span class="required">*</span></label>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <input type="text" id="cvv" name="cvv" maxlength="3" required class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans_choice('text.expiry_date',1,[],Auth()->user()->lang) }} <span class="required">*</span></label>
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="expiryM" name="expiryM" placeholder="MM" maxlength="2" required class="form-control col-md-3">
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="expiryY" name="expiryY" placeholder="YY" maxlength="2" required class="form-control col-md-3">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <select id="cardType" name="cardType" class="form-control" required="required">
                                <option class="default">Tipo de tarjeta</option>
                                <option value="visa">Visa</option>
                                <option value="mastercard">MasterCard</option>
                                <option value="discover">Discover</option>
                                <option value="amex">American Express</option>
                            </select>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="col-xs-8 col-sm-4 col-sm-offset-6 col-md-4 col-md-offset-8">
                        <button type="submit" id="actionPay" data-role="add" class="btn btn-lg btn-block btn-primary">
                            <i class="fa fa-spinner fa-spin loading" style="display: none;"></i>
                            {{ trans_choice('text.pay',1,[],Auth()->user()->lang) }}
                            <span class="amount"></span>
                        </button>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>

        <div class="col-md-8" id="divInfo">
            <div class="x_panel" style="margin-left: 3%;">
                <div class="page-title" style="margin-bottom: 3%">
                    <h6 class="landing_name"></h6>
                    <h1 class="name">Nombre Completo Programa</h1>
                </div>
                @can('program.update')
                    <div style="display:none; float: right" id="divVideo">
                        <button type="button" class="btn btn-round btn-lg btn-dark" title="Editar Video" data-toggle="modal" data-target="#modVideoProgram">
                            <i class="fa fa-refresh" aria-hidden="true"></i>
                        </button>
                    </div>
                @endcan
                <br>
                <div class="row">
                    <div style="padding-bottom: 62.5%; position: relative">
                        <iframe id="video" width="100%" height="100%"  frameborder="0" style="position: absolute; top: 0; left: 0;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                </div>
                <br>
                <blockquote>
                    <p class="description">Descripción completa y detallada del programa a ofrecer</p>
                </blockquote>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modVideoProgram" role="dialog" style="margin-top: 8em">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Actualizar Video</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <input type="url" id="newVideo" placeholder="Url del video completa" class="form-control">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-primary" id="btnVideo">Ir !</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @parent
    <script src="{{asset('js/program/learn.js')}}"></script>
@endsection