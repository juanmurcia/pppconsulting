@extends('layouts.app')

@section('content')
    <div id="util">
        <input type="hidden" id="programId" value="{{ $id }}">
        <input type="hidden" id="lessonId" value="{{ $lesson_id }}">
        <input type="hidden" id="classId" value="{{ $class_id }}">
    </div>
    <div class="" style="margin-top: 8em; margin-bottom: 3em">
        <div class="col-lg-3 col-lg-offset-1 col-md-3 col-xs-12 menu_fixed" style="margin-bottom: 2em">
            <div class="x_logo text-center" style="position:relative">
                <h3 class="landing_name" style="position:absolute; bottom: 0; left:0; width: 100%; text-align: center; text-shadow: 0 0 1em rgba(0,0,0,0.7)"></h3>
            </div>
            <div class="x_panel">
                <div class="row">
                    <div class="progressBar col-xs-8 col-xs-offset-2 text-center">
                        <div class="progress progress_sm">
                            <div id="progressBar" class="progress-bar bg-green" role="progressbar"></div>
                        </div>
                        <small><span id="advance"></span>% {{ trans_choice('text.complete',1,[],Auth()->user()->lang) }}</small>
                    </div>
                </div>
                <br>
                <div class="row">
                    @can('program.update')
                        <div style="display:none;" class="addItem text-center">
                            <button type="button" class="btn btn-lg btn-default text-right" title="Agregar Lección" onclick="add_lesson();">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                <span class="hidden-xs" style="margin-left: 16px">Lección</span>
                            </button>
                            <button type="button" class="btn  btn-lg btn-primary text-left" title="Agregar Clase" onclick="add_class();">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                <span class="hidden-xs" style="margin-left: 16px">Clase</span>
                            </button>
                        </div>
                    @endcan
                    <select name="optLesson" onchange="list_class(this.value)" id="optLesson" class="select2"></select>
                </div>
                <br>
                <div class="row">
                    <ul id="listClasses" class="to_do"></ul>
                </div>
            </div>
        </div>
        <div class="col-lg-7 col-md-9 col-xs-12" style="padding-bottom: 2em">
            <div class="x_panel">
                <div class="row">
                    <div class="col-sm-9 col-xs-12">
                        <h3><span class="program_name"></span>  <span class="lesson_name"></span></h3>
                        <h1 class="name"></h1>
                    </div>
                    <div class="col-sm-3 col-sm-offset-0 col-xs-8 col-xs-offset-2 ">
                        <br>
                        <a id="completeClass" class="btn btn-block btn-lg btn-success">{{ trans_choice('text.lesson_completed',1,[],Auth()->user()->lang) }}</a>
                        <div class="row">
                            @can('program.update')
                                <div class="col-md-12">
                                    <button type="button" id="btnEditLesson" class="btn btn-lg btn-block btn-default" title="Editar Clases" onclick="edit_lesson();">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                        <span class="hidden-xs" style="margin-left: 16px">Editar</span>
                                    </button>
                                </div>

                                <form style="display: none" action="#" id="frmAddProgram" name="frmAddProgram" onsubmit="return false;">
                                    <input type="text" id="class_video" name="class_video">
                                    {{ csrf_field() }}
                                </form>
                            @endcan
                        </div>
                    </div>
                </div>
                @can('program.update')
                    <div style="display:none;" id="divVideo">
                        <button type="button" style="z-index: 1000;" class="btn btn-round btn-lg btn-dark" title="Editar Video" data-toggle="modal" data-target="#modVideoProgram">
                            <i class="fa fa-refresh" aria-hidden="true"></i>
                        </button>
                    </div>
                @endcan
                <br>
                <div style="padding-bottom: 62.5%; position: relative">
                    <iframe id="video" width="100%" height="100%"  frameborder="0" style="position: absolute; top: 0; left: 0;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
                <br>
                <p class="description"></p>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modVideoProgram" role="dialog" style="margin-top: 8em">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Actualizar Video</h3>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <input type="url" id="newVideo" placeholder="Código del video" class="form-control">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-primary" id="btnVideo">Ir !</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @parent
    <script src="{{asset('js/program/lesson.js')}}"></script>
@endsection