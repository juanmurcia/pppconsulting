@extends('layouts.app')

@section('content')
    <div class="row" style="margin-top: 8%; margin-bottom: 3%">
        <div class="col-md-6 col-md-offset-3">
            <div class="x_panel" style="margin-left: 3%;">
                <div class="page-title text-center" style="margin-bottom: 3%">
                    <h1>
                        <span class="glyphicon glyphicon-ok-circle"></span><br>
                        {{ trans_choice('text.payed',2,[],Auth()->user()->lang) }}
                    </h1>
                </div>

                <br>
                <div class="col-xs-8 col-sm-3 col-sm-offset-6 col-md-4 col-md-offset-4">
                    <a href="{{ route('home') }}" class="btn btn-lg btn-block btn-primary">
                        {{ trans_choice('text.continue_program',1,[],Auth()->user()->lang) }}
                    </a>
                </div>
            </div>
        </div>

@endsection