<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>{{ trans_choice('text.process',2) }}</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-phone"></i> {{ trans_choice('text.service_request',2) }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a>{{ trans_choice('text.open_female',2) }}<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li class="sub_menu"><a href="#">{{ trans_choice('text.all_female',2) }}</a></li>
                            <li class="sub_menu"><a href="#">{{ trans_choice('text.preventive_male',2) }}</a></li>
                            <li class="sub_menu"><a href="#">{{ trans_choice('text.other_female',2) }}</a></li>
                        </ul>
                    </li>
                    <li><a href="#">{{ trans_choice('text.closed_female',2) }}</a></li>
                    <li><a href="#">{{ trans_choice('text.refused_female',2) }}</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-folder-open"></i> {{ trans_choice('text.service_order',2) }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a>{{ trans_choice('text.open_female',2) }}<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li class="sub_menu"><a href="#">{{ trans_choice('text.all_female',2) }}</a></li>
                            <li class="sub_menu"><a href="#">{{ trans_choice('text.preventive_male',2) }}</a></li>
                            <li class="sub_menu"><a href="#">{{ trans_choice('text.other_female',2) }}</a></li>
                        </ul>
                    </li>
                    <li><a href="#">{{ trans_choice('text.closed_female',2) }}</a></li>
                    <li><a href="#">{{ trans_choice('text.refused_female',2) }}</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-wrench"></i> {{ trans_choice('text.visit',2) }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="#">{{ trans_choice('text.open_female',2) }}</a></li>
                    <li><a href="#">{{ trans_choice('text.closed_female',2) }}</a></li>
                    <li><a href="#">{{ trans_choice('text.refused_female',2) }}</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="menu_section">
        <h3>{{ trans_choice('text.configuration',2) }}</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-cubes"></i> {{ trans_choice('text.resource',2) }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('user.index') }}">{{ trans_choice('text.user',2) }}</a></li>
                    <li><a href="{{ route('client.index') }}">{{ trans_choice('text.client',2) }}</a></li>
                    <li><a href="{{ route('client_site.index') }}">{{ trans_choice('text.client_site',2) }}</a></li>
                    <li><a href="{{ route('client_site_area.index') }}">{{ trans_choice('text.client_site_area',2) }}</a></li>
                    <li><a href="{{ route('equipment.index') }}">{{ trans_choice('text.equipment',2) }}</a></li>
                    <li><a href="{{ route('equipment_category.index') }}">{{ trans_choice('text.equipment_category',2) }}</a></li>
                    <li><a href="{{ route('equipment_brand.index') }}">{{ trans_choice('text.equipment_brand',2) }}</a></li>
                    <li><a href="{{ route('equipment_model.index') }}">{{ trans_choice('text.equipment_model',2) }}</a></li>
                    <li><a href="#">{{ trans_choice('text.equipment_risk',2) }}</a></li>
                    <li><a href="{{ route('spare_part.index') }}">{{ trans_choice('text.spare_part',2) }}</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>