<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }}</title>    <!-- Final -->
    <link href="{{ asset('/css/consulting.css') }}" rel="stylesheet">
    @yield('style')
</head>
<body class="nav-md footer_fixed">
<div class="body" style="overflow: hidden">    <!-- top navigation -->
    <div class="top_nav">
        <div class="nav_menu" style="left: 0; top: 0">
            <nav>
                <div class="pull-left text-center"><a href="/"><img src="{{ asset('/images/logo.svg') }}"
                                                                    style="height: 3em; margin: 1em"></a></div>
                <ul class="nav navbar-nav navbar-right" style="width: auto !important;">
                    <li role="presentation" class="dropdown"><a href="javascript:;" class="dropdown-toggle info-number"
                                                                data-toggle="dropdown" aria-expanded="false"> <i
                                    class="glyphicon glyphicon-envelope" style="color: #fff;" aria-hidden="true"></i>
                            <span class="badge bg-green">1</span> </a>
                        <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                            <li><a> <span class="image"><img src="/images/img.jpg" alt="Profile Image"/></span> <span>                                            <span>John Smith</span>                                            <span
                                                class="time">152 mins ago</span>                                        </span>
                                    <span class="message">                                            Film festivals used to be do-or-die moments for movie makers. They were where...                                        </span>
                                </a></li>
                            <li>
                                <div class="text-center"><a> <strong>See All Alerts</strong> <i
                                                class="fa fa-angle-right"></i> </a></div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="user-profile dropdown-toggle" style="cursor: pointer" data-toggle="dropdown" aria-expanded="false">
                            <h6 style="color: #fff;">
                                <i class="glyphicon glyphicon-user" aria-hidden="true"></i>
                                &nbsp;{{ Auth::user()->name }} <i class="glyphicon glyphicon-menu-down" aria-hidden="true"></i>
                            </h6>
                        </a>
                        <ul class="dropdown-menu dropdown-usermenu pull-right">
                            <li><a href="/"> <i
                                            class="fa fa-suitcase pull-right"></i> {{ trans_choice('text.program',2,[],Auth()->user()->lang) }}
                                </a></li>
                            <li><a href="/profile"> <i
                                            class="fa fa-user pull-right"></i> {{ trans_choice('text.profile',1,[],Auth()->user()->lang) }}
                                </a></li>
                            <li><a id="btnLogout" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <i
                                            class="fa fa-sign-out pull-right"></i> {{ trans_choice('text.log_out',1,[],Auth()->user()->lang) }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">@csrf</form>
                            </li>
                        </ul>
                    </li>
                    @can("user.index")
                        <li>
                            <a href="{{ route('user.index') }}" class="user-profile dropdown-toggle" style="cursor: pointer" >
                                <h6 style="color: #fff;">
                                    <i class="glyphicon glyphicon-knight" aria-hidden="true"></i>
                                    Usuarios
                                </h6>
                            </a>
                        </li>
                    @endcan
                </ul>
            </nav>
        </div>
    </div>    <!-- /top navigation -->    <!-- page content -->@yield('content')<!-- /page content -->
    <!-- page modals -->@yield('modals')@include('audit.table')<!-- /page modals --><!-- footer content        <footer>            <div class="pull-right">                <p>{!! __('text.copy_right_lg') !!}</p>            </div>            <div class="clearfix"></div>        </footer>         /footer content -->
</div><!-- Final -->
<script src="{{ asset('/js/consulting.js') }}"></script><!-- Google Analytics -->
<script>    /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');    ga('create', 'UA-23581568-13', 'auto');    ga('send', 'pageview');*/</script>@yield('script')
</body>
</html>