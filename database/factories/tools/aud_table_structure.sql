DROP PROCEDURE IF EXISTS `aud_table_structure`;

CREATE PROCEDURE `aud_table_structure`(IN `s_table` VARCHAR(250), IN `s_db` VARCHAR(100)) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
BEGIN
	DECLARE i_exist INT DEFAULT 0;
	DECLARE s_aud_table VARCHAR(200);
	DECLARE s_name TEXT;
	DECLARE s_type TEXT;
	DECLARE t_field TEXT DEFAULT ' ';
	DECLARE done INT DEFAULT 0;
	DECLARE cur1 CURSOR FOR
		SELECT COLUMN_NAME, COLUMN_TYPE FROM information_schema.COLUMNS
		WHERE TABLE_SCHEMA = s_db AND TABLE_NAME = s_table AND COLUMN_NAME NOT IN ('user_cre_id', 'user_mod_id','created_at', 'updated_at');
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

	SET s_aud_table = CONCAT('audit_',s_table);
	SELECT COUNT(*) INTO i_exist FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = s_db AND TABLE_NAME = s_aud_table;

	IF i_exist = 0 THEN
		OPEN cur1;
			my_cur_loop:
			LOOP FETCH cur1 INTO s_name, s_type;
				IF done = 1 THEN
					LEAVE my_cur_loop;
				END IF;

				SET t_field = CONCAT(t_field,s_name,' ',s_type,',');
			END LOOP;
		CLOSE cur1;

		SET @query = CONCAT('CREATE TABLE ',s_aud_table,' (
			audit_id INT UNSIGNED NOT NULL AUTO_INCREMENT, ',
			t_field,
			'user_cre_id INT,
			created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (audit_id)
		);');
		PREPARE QUERY FROM @query;
		EXECUTE QUERY;
		DEALLOCATE PREPARE QUERY;
	END IF;
END