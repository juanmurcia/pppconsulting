DROP PROCEDURE IF EXISTS `aud_table_structure`;
CREATE PROCEDURE `aud_table_structure`(IN `s_table` VARCHAR(250), IN `s_db` VARCHAR(100)) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
BEGIN
  DECLARE i_exist INT DEFAULT 0;
  DECLARE s_aud_table VARCHAR(200);

  SET s_aud_table = CONCAT('audit_',s_table);
  SELECT COUNT(*) INTO i_exist FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = s_db AND TABLE_NAME = s_aud_table;

  IF i_exist = 0 THEN
    SET @query = CONCAT('CREATE TABLE ',s_aud_table,' ( audit_id INT UNSIGNED NOT NULL AUTO_INCREMENT, id_parent int,  field VARCHAR(250), old_value TEXT, new_value TEXT, user_cre_id INT, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (audit_id));');
    PREPARE QUERY FROM @query;
    EXECUTE QUERY;
    DEALLOCATE PREPARE QUERY;
  END IF;
END