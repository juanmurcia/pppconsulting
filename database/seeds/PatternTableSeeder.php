<?php

use Illuminate\Database\Seeder;
use App\Models\Pattern;

class PatternTableSeeder extends Seeder
{
    public function run()
    {
        /*
        $pattern = new Pattern();
        $pattern->id=1;
        $pattern->name = 'Cliente';
        $pattern->model = 'client';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=2;
        $pattern->name = 'Empresa';
        $pattern->model = 'company';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=3;
        $pattern->name = 'Usuario';
        $pattern->model = 'user';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=4;
        $pattern->name = 'Equipo';
        $pattern->model = 'equipment';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=5;
        $pattern->name = 'Modelo Equipo';
        $pattern->model = 'equipmentModel';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=6;
        $pattern->name = 'Marca Equipo';
        $pattern->model = 'equipmentBrand';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=7;
        $pattern->name = 'Categoría Equipo';
        $pattern->model = 'equipmentCategory';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=8;
        $pattern->name = 'Area Sede Cliente';
        $pattern->model = 'clientSiteArea';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=9;
        $pattern->name = 'Protocolo Mantenimiento';
        $pattern->model = 'maintenanceProtocol';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=10;
        $pattern->name = 'Riego Equipo';
        $pattern->model = 'equipmentRisk';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=11;
        $pattern->name = 'Sede Cliente';
        $pattern->model = 'clientSite';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=12;
        $pattern->name = 'País';
        $pattern->model = 'country';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=13;
        $pattern->name = 'Estados Pais';
        $pattern->model = 'countryState';
        $pattern->save();

        $pattern = new Pattern();
        $pattern->id=14;
        $pattern->name = 'Ciudades Pais';
        $pattern->model = 'countryCity';
        $pattern->save();*/

        $pattern = new Pattern();
        $pattern->id=15;
        $pattern->name = 'Repuestos';
        $pattern->model = 'sparePart';
        $pattern->save();
    }
}
