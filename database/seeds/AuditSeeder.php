<?php

use Illuminate\Database\Seeder;
use App\Http\Controllers\TriggerController;

class AuditSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'database/factories/tools/aud_table_structure.sql';
        DB::unprepared(file_get_contents($path));

        $trigger = new TriggerController();
        $models = $trigger->autoModel();

        foreach($models as $model){

            DB::unprepared("CALL aud_table_structure ('$model->model', '".env('DB_DATABASE')."'); ");
            if(!in_array($model->model,[''])){
                $sql = $trigger->make($model->model, true);
                DB::unprepared($sql);
            }

        }
    }
}
