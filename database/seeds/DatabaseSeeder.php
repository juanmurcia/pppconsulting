<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(AuditSeeder::class);
        //$this->call(PatternTableSeeder::class);
        //$this->call(StateTableSeeder::class);
        //$this->call(UserTableSeeder::class);
    }
}
