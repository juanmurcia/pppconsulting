<?php

use Illuminate\Database\Seeder;
use App\Models\State;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$state = new State();
        $state->id=1;
        $state->name = 'Activo';
        $state->pattern_id = 1;
        $state->save();

        $state = new State();
        $state->id=2;
        $state->name = 'Inactivo';
        $state->pattern_id = 1;
        $state->save();
        // Company
        $state = new State();
        $state->id=3;
        $state->name = 'Activo';
        $state->pattern_id = 2;
        $state->save();

        $state = new State();
        $state->id=4;
        $state->name = 'Inactivo';
        $state->pattern_id = 2;
        $state->save();

        // User
        $state = new State();
        $state->id=5;
        $state->name = 'Activo';
        $state->pattern_id = 3;
        $state->save();

        $state = new State();
        $state->id=6;
        $state->name = 'Inactivo';
        $state->pattern_id = 3;
        $state->save();

        // Equipment
        $state = new State();
        $state->id=7;
        $state->name = 'Activo';
        $state->pattern_id = 4;
        $state->save();

        $state = new State();
        $state->id=8;
        $state->name = 'Inactivo';
        $state->pattern_id = 4;
        $state->save();

        // equipmentModel
        $state = new State();
        $state->id=9;
        $state->name = 'Activo';
        $state->pattern_id = 5;
        $state->save();

        $state = new State();
        $state->id=10;
        $state->name = 'Inactivo';
        $state->pattern_id = 5;
        $state->save();

        // equipmentBrand
        $state = new State();
        $state->id=11;
        $state->name = 'Activo';
        $state->pattern_id = 6;
        $state->save();

        $state = new State();
        $state->id=12;
        $state->name = 'Inactivo';
        $state->pattern_id = 6;
        $state->save();

        // equipmentCategory
        $state = new State();
        $state->id=13;
        $state->name = 'Activo';
        $state->pattern_id = 7;
        $state->save();

        $state = new State();
        $state->id=14;
        $state->name = 'Inactivo';
        $state->pattern_id = 7;
        $state->save();

        // maintenanceProtocol
        $state = new State();
        $state->id=15;
        $state->name = 'Activo';
        $state->pattern_id = 9;
        $state->save();

        $state = new State();
        $state->id=16;
        $state->name = 'Inactivo';
        $state->pattern_id = 9;
        $state->save();

        // equipmentRisk
        $state = new State();
        $state->id=17;
        $state->name = 'Activo';
        $state->pattern_id = 10;
        $state->save();

        $state = new State();
        $state->id=18;
        $state->name = 'Inactivo';
        $state->pattern_id = 10;
        $state->save();

        // clientSite
        $state = new State();
        $state->id=19;
        $state->name = 'Activo';
        $state->pattern_id = 11;
        $state->save();

        $state = new State();
        $state->id=20;
        $state->name = 'Inactivo';
        $state->pattern_id = 11;
        $state->save();


        // countryState
        $state = new State();
        $state->id=21;
        $state->name = 'Activo';
        $state->pattern_id = 13;
        $state->save();

        $state = new State();
        $state->id=22;
        $state->name = 'Inactivo';
        $state->pattern_id = 13;
        $state->save();

        // countryCity
        $state = new State();
        $state->id=23;
        $state->name = 'Activo';
        $state->pattern_id = 14;
        $state->save();

        $state = new State();
        $state->id=24;
        $state->name = 'Inactivo';
        $state->pattern_id = 14;
        $state->save();

        // clientSiteArea
        $state = new State();
        $state->id=25;
        $state->name = 'Activo';
        $state->pattern_id = 8;
        $state->save();

        $state = new State();
        $state->id=26;
        $state->name = 'Inactivo';
        $state->pattern_id = 8;
        $state->save();

        // country
        $state = new State();
        $state->id=27;
        $state->name = 'Activo';
        $state->pattern_id = 12;
        $state->save();

        $state = new State();
        $state->id=28;
        $state->name = 'Inactivo';
        $state->pattern_id = 12;
        $state->save();*/

        // SparePart
        $state = new State();
        $state->id=29;
        $state->name = 'Activo';
        $state->pattern_id = 14;
        $state->save();

        $state = new State();
        $state->id=30;
        $state->name = 'Inactivo';
        $state->pattern_id = 14;
        $state->save();
    }
}
