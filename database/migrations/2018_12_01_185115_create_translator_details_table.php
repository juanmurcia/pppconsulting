<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslatorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translator_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pattern_id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->string('field');
            $table->string('lang');
            $table->text('text')->nullable();
            $table->timestamps();
            //Key
            $table->foreign('pattern_id')->references('id')->on('patterns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translator_details');
    }
}
