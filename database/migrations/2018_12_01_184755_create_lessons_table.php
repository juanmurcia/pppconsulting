<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->integer('order')->unsigned();
            $table->string('name');
            $table->integer('user_cre_id')->unsigned();
            $table->integer('user_mod_id')->unsigned();
            $table->timestamps();
            //Key
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('user_cre_id')->references('id')->on('users');
            $table->foreign('user_mod_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
