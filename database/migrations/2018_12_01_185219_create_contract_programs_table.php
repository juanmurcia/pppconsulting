<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_programs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->unsigned();
            $table->integer('license_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('advance')->default(0);
            $table->timestamps();
            //Key
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('license_id')->references('id')->on('licenses');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_programs');
    }
}
