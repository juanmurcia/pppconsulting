<?php


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\Http\Controllers\TriggerController;

class CreateAuditTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $path = 'database/factories/tools/aud_table_structure.sql';
        DB::unprepared(file_get_contents($path));

        $trigger = new TriggerController();
        $models = $trigger->autoModel();

        foreach($models as $model){

            DB::unprepared("CALL aud_table_structure ('$model->model', '".env('DB_DATABASE')."'); ");
            if(!in_array($model->model,['people','users','billings','billin_details'])){
                $sql = $trigger->make($model->model, true);
                DB::unprepared($sql);
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $trigger = new TriggerController();
        $models = $trigger->autoModel();

        foreach($models as $model){
            DB::unprepared("DROP TABLE IF EXISTS aud_".$model->model);
        }
    }
}
