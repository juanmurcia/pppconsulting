<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvanceProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_programs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contract_program_id')->unsigned();
            $table->integer('class_id')->unsigned();
            $table->boolean('completed')->default(false);
            $table->timestamps();
            //Key
            $table->foreign('contract_program_id')->references('id')->on('contract_programs');
            $table->foreign('class_id')->references('id')->on('classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advance_programs');
    }
}
