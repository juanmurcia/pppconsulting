var tbContract = $('#tbContract').DataTable({
    responsive: true,
    searching: false,
    lengthMenu: [[25,50,100, -1], [25,50, 100, "Todo"]],
    columns: [
        {data: 'id', name: 'id', className: "text-center"},
        {data: 'name', name: 'name'},
        {data: 'advance', name: 'advance', className: "text-center"},
        {data: 'buttons', name: 'buttons', className: "text-center"}
    ]
});


var columns = [
    {data: "role", name: "role", column: '' , visible: true},
    {data: "name", name: "name", column: '' , visible: true},
    {data: "last_name", name: "last_name", column: '' , visible: true},
    {data: "email", name: "email", column: '', visible: true},
    {data: "phone", name: "phone", column: '', visible: true},
    {data: "buttons", name: "buttons", className: "text-center", orderable: false, searchable: false}
]

$("#tbUser thead th").each(function (index) {
    if(columns[index].hasOwnProperty('column')) {
        columns[index].column = $(this).text();
    }
});
//Pasar Columnas a Auditoría
columnsAudit = columns;

function tableUser(){
    tableSearch({
        id:"tbUser",
        type:"GET",
        url:"/user",
        serverSide: true,
        destroy : true,
        checkInactive : true,
        exportButtons : true,
        showHide: true,
        columns: columns
    });
}
tableUser();

function destroyUser(id) {
    if(confirm($('.btn-inactivate').data('message'))){
        requestAjax({
            type: "DELETE",
            url: "/user/"+id,
            form: "frmUser",
            success: "Registro Inactivado",
            function: 'tableUser'
        });
    }
}

function activateUser(id) {
    if(confirm($('.btn-activate').data('message'))){
        requestAjax({
            type: "POST",
            url: "/user/"+id,
            form: "frmUser",
            success: "Registro Activado",
            function: 'tableUser'
        });
    }
}

function editUser(id) {
    var response = requestAjax({
        type: "GET",
        url: "/user/"+id,
        form: "frmUser",
        return: true,
    });

    $("#actionUser").data("role","change");
    $("#actionUser").data("id",id);
    $("#frmUser")[0].reset();
    $('#divPasswords').hide();

    //Carga Formulario
    completeForm('frmUser',response);
    $('#phone').removeTag("");
}

function contract_user(id){
    var response = requestAjax({
        type: "GET",
        url: "/contract_user/"+id,
        return: true,
    });
    $("#actionContract").data("user",id);

    $.when(response).done(function () {
        if(response.data){
            tbContract.clear().draw();
            $.each(response.data, function (index, val) {

                addColumnDataTable(tbContract,{
                    "id":  val.id,
                    "name":  val.program.name,
                    "advance":  '<div class="progress progress_sm">' +
                        '<div class="progressBar progress-bar bg-green" data-transitiongoal="'+val.advance+'" role="progressbar"></div>' +
                    '</div>'+
                    '<span>'+val.advance+' %</span>',
                    "buttons" : '<button class="btn btn-danger btn-xs" onclick="delContract('+val.id+', '+id+');"><i class="fa fa-close"></i></button>',
                });
            });

            $('.progress .progress-bar').progressbar();
        }
    });
}

function delContract(id, user_id){
    if(confirm("Seguro desea inactivar el contrato del usuario ?")) {
        var response = requestAjax({
            type: "DELETE",
            url: "/contract_user/" + id,
            form: "frmContract",
            return: true,
        });

        $.when(response).done(function () {
            $('#frmContract')[0].reset();

            if (response.success) {
                contract_user(user_id);
            } else {
                alert(response.msj);
            }
        });
    }
}

$("#actionContract").click(function(){
    var user_id = $(this).data("user");

    var response = requestAjax({
        type: "POST",
        url: "/contract_user/"+user_id,
        form: "frmContract",
        return: true,
    });

    $.when(response).done(function () {
        if (response.success) {
            $('#frmContract')[0].reset();
            contract_user(user_id);
        }else{
            alert(response.msj);
        }
    });
});