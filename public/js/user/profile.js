$("#"+$("#lang").val()).append("<span class='langChecked badge bg-green'><i class='fa fa-check'></i></span>");

function select_lang(obj){
    var lang = $(obj).data("lang");

    $(".langChecked").empty();

    $("#lang").val(lang);
    $("#"+lang).append("<span class='langChecked badge bg-green'><i class='fa fa-check'></i></span>");
}

function form_profile(type){
    $(".profile").hide();
    $(".messages .message").removeClass("bg-blue-sky");
    $("#ty"+type).addClass("bg-blue-sky");
    switch (type){
        case 1:
            $("#profile").show();
            break;
        case 2:
            $("#profilePass").show();
            break;
    }


}

form_profile(1);

$("#actionUser").click(function () {
    var response = requestAjax({
        type: "PUT",
        url: "/user_profile",
        form: "frmUser",
        success: "Registro Modificado",
        return: true
    });

    if(response.success){
        $(location).attr("href","/profile");
    }
});

$("#actionPassword").click(function () {
    var response = requestAjax({
        type: "POST",
        url: "/user_password",
        form: "frmPassword",
        success: "Registro Modificado",
        return: true
    });

    if(response.success){
        $("#btnLogout").click();
    }
});
