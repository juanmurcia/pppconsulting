function createUser() {
    $("#frmUser")[0].reset();
    $("#actionUser").data("role","add");
    $('#divPasswords').show();
}

$("#actionUser").click(function() {
    var role = $(this).data("role");
    hideTime("#actionUser");

    if(role == "add"){
        requestAjax({
            type: "POST",
            url: "/user",
            modal: "modUser",
            form: "frmUser",
            success: "Registro creado",
            function: 'tableUser'
        });

    }else if(role== "change"){
        var id = $(this).data("id");
        requestAjax({
            type: "PUT",
            url: "/user/"+id,
            modal: "modUser",
            form: "frmUser",
            success: "Registro Modificado",
            function: 'tableUser'
        });
    }
});

$("#actionRegister").click(function() {
    hideTime("#actionRegister");

    var response = requestAjax({
        type: "POST",
        url: "/register",
        form: "frmRegister",
        return: true
    });

    $.when(response).done(function () {
        if(response.success){
            $(location).attr("href","/confirmed_register");
        }else{
            alert(response.errorBd);
            //$(location).attr("href","/account");
        }
    });
});

