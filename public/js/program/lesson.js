var id = $("#programId").val();
var lesson_id = $("#lessonId").val();
var class_id = $("#classId").val();
$("#util").empty();

var responseG = requestAjax({
    type: "GET",
    url: "/programs/" + id,
    return: true
});

if(responseG.length == 0){
    $(location).attr('href',"/");
}

//Valida si esta completa la petición
if(lesson_id > 0 && class_id > 0){
    $(".landing_name").text(responseG.landing_name);
    $(".program_name").html("<small>"+responseG.name+" / </small>");
    $("#advance").text(responseG.advance);
    $("#progressBar").attr('data-transitiongoal', responseG.advance);
    $(".x_logo").attr('style', "background: url('/images/"+responseG.landing_image+"') ;");
    $('.progress .progress-bar').progressbar();

    responseG.lessons.sort(function (a, b) {
        if (a.order > b.order) {
            return 1;
        }
        if (a.order < b.order) {
            return -1;
        }
        return 0;
    });

    $.each(responseG.lessons, function (index, val) {
        var selected = val.id == lesson_id ? true : false;
        $('#optLesson')
            .append($("<option></option>")
                .attr("value",val.id)
                .attr("selected",selected)
                .text(val.name));
    });
    list_class(lesson_id);
    show_class(lesson_id, class_id);
}else{
    var lesson = responseG.lessons.find(function(element){return element.order == 1;});
    var classes = list_class(lesson.id);
    var classes = classes.find(function(element){return element.order == 1;});

    $(location).attr('href',"/programs/"+id+"/progress/"+lesson.id+"/"+classes.id);
}

$("#btnVideo").click(function(){
    $("#class_video").val($("#newVideo").val());
    $("#video").attr('src',"https://player.vimeo.com/video/"+$("#newVideo").val());

    $('#modVideoProgram').modal('hide');
});

function show_class(lessonId, classId) {
    var lesson = responseG.lessons.find(function(element){return element.id == lessonId;});
    var response = requestAjax({
        type: "GET",
        url: "/classes/"+classId,
        return: true
    });

    $(".lesson_name").html("<small>"+lesson.name+"</small>");
    $(".name").text(response.name);
    $(".description").html(response.description);
    $("#video").attr('src',"https://player.vimeo.com/video/"+response.info_video);
    $("#class_video").val(response.info_video);

}

function list_class(lessonId){
    lesson_id = lessonId;
    var response = requestAjax({
        type: "GET",
        url: "/lesson/" + lessonId+"/"+id+"/class",
        return: true
    });

    $('#listClasses').empty();
    var check = '';
    var text = '';
    $.each(response, function (index, val) {
        check = val.completed ? "<span style='float: right; width: 1.5em; height:1.5em; line-height: 1.5em; text-align: center; background-color: #00BE49; border-radius: 50%; color: #ffffff;'><i class='glyphicon glyphicon-ok'></i></span>" : '';
        text = val.id == class_id ? "<i class='fa fa-folder-open-o'></i>  <strong>"+val.name+"</strong>" : val.name;

        $('#listClasses').append("<li onclick='select_class("+val.id+")' style='cursor: pointer;'><p>"+text+check+"</p></li>");
    });

    return response;
}

function select_class(classId){
    $(location).attr('href',"/programs/"+id+"/progress/"+lesson_id+"/"+classId);
}

$("#completeClass").click(function () {
    var response = requestAjax({
        type: "GET",
        url: "/classes/"+class_id+"/"+id+"/completed",
        return : true
    });

    $.when(response).done(function () {
        if(response.success){
            select_class(class_id);
        }
    });
})


function edit_lesson(){
    $('.name, .lesson_name, .description').attr('contenteditable', 'true');
    $("#divVideo, .addItem").show();

    $("#btnEditLesson").unbind();
    $("#btnEditLesson").addClass("disabled");
    $("#completeClass").removeClass("btn-success");
    $("#completeClass").addClass("btn-warning");
    $("#completeClass").html('<i class="fa fa-edit" aria-hidden="true"></i> Guardar');
    $("#completeClass").on("click",function(){ save_class(); });
}

function save_class(){
    var lesson_name = $(".lesson_name").text();
    var name = $(".name").text();
    var description = $(".description").text();


    $("#frmAddProgram").append("<input type='hidden' id='lesson_name' name='lesson_name' value='"+lesson_name+"'>" +
        "<input type='hidden' id='name' name='name' value='"+name+"'>" +
        "<input type='hidden' id='description' name='description' value='"+description+"'>");

    requestAjax({
        type: "PUT",
        url: "/lesson/"+lesson_id,
        form: "frmAddProgram",
        return : true
    });

    var response = requestAjax({
        type: "PUT",
        url: "/class/"+class_id,
        form: "frmAddProgram",
        success: "Clase actualizada",
        return : true
    });

    $.when(response).done(function () {
        if(response.success){
            select_class(class_id);
        }
    });
}

function add_lesson(){
    var response = requestAjax({
        type: "POST",
        url: "/lesson/"+id,
        form: "frmAddProgram",
        success: "Lección Agregada",
        return : true
    });

    $.when(response).done(function () {
        if(response.success){
            $('#optLesson').append($("<option></option>").attr("value",response.data.id).text(response.data.name));
        }
    });
}

function add_class(){
    var response = requestAjax({
        type: "POST",
        url: "/class/"+lesson_id,
        form: "frmAddProgram",
        success: "Clase agregada",
        return : true
    });

    $.when(response).done(function () {
        if(response.success){
            $('#listClasses').append("<li onclick='select_class("+response.data.id+")' style='cursor: pointer;'><p>"+response.data.name+"</p></li>");
        }
    });
}