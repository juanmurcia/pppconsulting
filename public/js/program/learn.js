var id = $("#programId").val();
var response = requestAjax({
    type: "GET",
    url: "/programs/" + id,
    return: true
});

$(".landing_name").text(response.landing_name);
$(".amount").text("   US"+response.cost);
$(".cost").text(response.cost);

if(response.name != null) {
    $(".name").text(response.name);
}
if(response.description != null) {
    $(".description").html(response.description);
}

if(response.landing_video != null) {
    $("#video").attr('src',response.landing_video);
}else{
    $("#video").attr('src',"https://player.vimeo.com/video/302344978");
}

if(response.landing_image != null) {
    $(".x_logo").attr("style","background :  url('/images/"+response.landing_image+"')");
}else {
    $(".x_logo").attr("style", "background :  url('/images/empty-photo.jpg')");
}

$("#btnVideo").click(function(){
    $("#landing_video").val($("#newVideo").val());
    $("#video").attr('src',$("#newVideo").val());

    $('#modVideoProgram').modal('hide');
});

function next_program(){
    $(location).attr('href',"/programs/"+id+"/progress");
}

function edit_program(){
    $('.name, .landing_name, .description, .cost').attr('contenteditable', 'true');
    $("#frmComplete").show();
    $("#divVideo").show();

    if(response.state_id == 13){
        $("#optEstProgram").val(20);
    }else{
        $("#optEstProgram").val(response.state_id);
    }
    $("#landing_description").val(response.landing_description);
    $.each(JSON.parse(response.lang), function(index, val) {
        $("#chkLang"+val.lang).attr('checked', true);
    });

    $("#btnEditProgram").unbind();
    $("#btnNextProgram").unbind();
    $("#btnEditProgram").addClass("disabled");
    $("#btnNextProgram").addClass("btn-warning");
    $("#btnNextProgram").html('<i class="fa fa-edit" aria-hidden="true"></i> Guardar');
    $("#btnNextProgram").on("click",function(){ save_program(); });
}

function save_program(){
    var landing_name = $(".landing_name").text();
    var name = $(".name").text();
    var description = $(".description").text();
    var landing_description = $("#landing_description").val();
    var state = $("#optEstProgram").val();
    var cost = $(".cost").text();

    var selected = [];
    $('#frmComplete input:checked').each(function() {
        selected.push({"lang" : $(this).val()});
    });

    var jsonLang = JSON.stringify(selected);

    $("#frmAddProgram").append("<input type='hidden' id='name' name='name' value='"+name+"'>" +
        "<input type='hidden' id='landing_name' name='landing_name' value='"+landing_name+"'>" +
        "<input type='hidden' id='landing_description' name='landing_description' value='"+landing_description+"'>" +
        "<input type='hidden' id='description' name='description' value='"+description+"'>" +
        "<input type='hidden' id='cost' name='cost' value='"+cost+"'>" +
        "<input type='hidden' id='state' name='state' value='"+state+"'>" +
        "<input type='hidden' id='lang' name='lang' value='"+jsonLang+"'>");

    var response = requestAjax({
        type: "PUT",
        url: "/program/"+id,
        form: "frmAddProgram",
        success: "Programa Creado",
        return : true
    });

    $.when(response).done(function () {
        if(response.success){
            $(location).attr('href',"/programs/"+id+"/learn");
        }else{
            alert(response.msj);
        }
    });
}

$("#actionPay").click(function(){
    $(".loading").show();

    var response = requestAjax({
        type: "POST",
        url: "/program/"+id+"/pay",
        form: "frmPayProgram",
        return : true
    });

    $.when(response).done(function () {
        $(".loading").hide();
        if(response.success){
            $(location).attr("href","/program_payed");
        }
    });
});

$("#billing").click(function () {
    $("#divBilling").show();
})