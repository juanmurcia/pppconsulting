var response = requestAjax({
    type: "GET",
    url: "/programs/list",
    return: true
});

var i = 0;
$.each(response, function (index, request) {
    var div = $("#divProgramCopy").clone();

    if(i == 0){
        $(div).addClass("active");
    }

    $(div).removeAttr("id");
    $(div).find(".title").text(request.landing_name);
    $(div).find(".description").text(request.landing_description);
    $(div).find(".xLogo").attr("style","background :  url('/images/"+request.landing_image+"')");


    $(div).find(".langProgram").empty();
    $.each($.parseJSON(request.lang), function (index, val) {
        $(div).find(".langProgram").append('<span style="color: white;background-color: #bbb;border-radius: 50%;margin:1em 0;height: 2em;width: 2em; line-height:2em; text-align:center; display: inline-block;">'+val.lang.toUpperCase()+'</span>&nbsp;');
    });

    if (request.state_id == 23) {
        $(div).find(".advance").text(request.advance);
        $(div).find(".progressBar").attr('data-transitiongoal', request.advance);
        $(div).find(".divProgress").show();
        $(div).find(".divBlock").hide();
        $(div).find(".linkProgram").addClass('btn-primary');
        $(div).find(".linkProgram").text($("#continue").text());
        $(div).find(".linkProgram").attr('href',"/programs/"+request.id+"/progress");
    }else if(request.state_id == 20) {
        $(div).find(".divBlock").show();
        $(div).find(".divProgress").hide();
        $(div).find(".linkProgram").addClass('btn-success');
        $(div).find(".linkProgram").text($("#learn").text());
        $(div).find(".linkProgram").attr('href',"/programs/"+request.id+"/learn");
    }else if(request.state_id == 21) {
        $(div).find(".divBlock").show();
        $(div).find(".divProgress").hide();
        $(div).find(".linkProgram").addClass('btn-dark');
        $(div).find(".linkProgram").text($("#coming").text());
        $(div).find(".linkProgram").attr('href',"#");
    }

    $("#detailPrograms").append(div);
    i = 1;
});

$('.progress .progress-bar').progressbar();

function new_program(){
    var div = $("#divProgramCopy").clone();
    var divComplete = $("#frmComplete").clone();

    $(div).find('.title, .description').attr('contenteditable', 'true');
    $(div).find(".divBlock").hide();
    $(div).find(".divProgress").hide();
    $(div).find(".title").text("Nombre Corto Programa");
    $(div).find(".description").text("Agregar descripción principal de este programa, luego podrá agregar una descripción mas detallada");
    $(div).find(".xLogo").attr("style","background :  url('/images/empty-photo.jpg')");

    $(div).find(".linkProgram").addClass('btn-warning');
    $(div).find(".linkProgram").text("Guardar");
    $(div).find(".linkProgram").attr('href',"#");
    $(div).find(".linkProgram").click(function () { create_program() });

    $(div).find(".language").empty();
    $(div).find(".frmComplete").append(divComplete);
    $("#detailPrograms").append(div);
}

function create_program(){
    var title = $("#divProgramCopy").find(".title").text();
    var description = $("#divProgramCopy").find(".description").text();
    var state = $("#divProgramCopy").find("#optEstProgram").val();

    var selected = [];
    $('#frmComplete input:checked').each(function() {
        selected.push({"lang" : $(this).val()});
    });

    var jsonLang = JSON.stringify(selected);

    $("#frmAddProgram").append("<input type='hidden' id='name' name='name' value='"+title+"'>" +
        "<input type='hidden' id='description' name='description' value='"+description+"'>" +
        "<input type='hidden' id='state' name='state' value='"+state+"'>" +
        "<input type='hidden' id='lang' name='lang' value='"+jsonLang+"'>");

    var response = requestAjax({
        type: "POST",
        url: "/program",
        form: "frmAddProgram",
        success: "Programa Creado",
        return : true
    });

    $.when(response).done(function () {
        if(response.success){
            console.log("recarga");
            $(location).attr('href',"/programs/"+response.id+"/learn");
        }
    });
}

if($.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))){
    $('.slick-inner').slick({
        slidesToShow: 1,
        slidesToScroll: 1
    });
}else{
    $('.slick-inner').slick({
        slidesToShow: 3,
        slidesToScroll: 1
    });
}
