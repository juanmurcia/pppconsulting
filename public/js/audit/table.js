let columnsAudit = [];
let buildAudit = true;
var initColumn = [
    {data: "created_at", name: "created_at", column: ''},
    {data: "username", name: "username", column: '' },
    {data: "state_id", name: "state_id", column: '' },
];

$("#tbAudit thead th").each(function (index) {
    initColumn[index].column = $(this).text();
});

var fnlColumnAudit = initColumn;

function audit(pattern_id, model_id){
    if(buildAudit){
        $( ".completeAudit" ).empty();
        $.each(initColumn, function(index, val) {
            $( ".completeAudit" ).append('<th>'+val.column+'</th>');
        });

        $.each(columnsAudit, function(index, val) {
            if(!val.hasOwnProperty('auditShow') || val.auditShow == false){
                if(val.hasOwnProperty('column')) {
                    $( ".completeAudit" ).append('<th>'+val.column+'</th>');
                }
                if(val.hasOwnProperty('auditColumn')) {
                    fnlColumnAudit.push({data: val.auditColumn, name: val.auditColumn});
                }else if (val.data != 'buttons') {
                    fnlColumnAudit.push(val);
                }
            }
        });
    }



    tableSearch({
        id:"tbAudit",
        type:"GET",
        url: '/audit/'+pattern_id+'/'+model_id,
        destroy : true,
        responsive: false,
        columns: fnlColumnAudit
    });
    buildAudit = false;
    $('#modAudit').modal({show: 'true'});
}