/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2017 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function (document, window, $) {
    'use strict';

    var Site = window.Site;

    $(document).ready(function ($) {
        $.fn.dataTable.ext.sErrMode = 'console.log';

        $.fn.dataTable.ext.type.order['salary-grade-pre'] = function (d) {
            switch (d) {
                case 'Low':
                    return 1;
                case 'Medium':
                    return 2;
                case 'High':
                    return 3;
            }
            return 0;
        };

        Site.run();
    });
});

var makeColumns = true;

function numberFormat(number, decimals, dec_point, thousands_sep) {
    // http://kevin.vanzonneveld.net
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     bugfix by: Michael White (http://getsprink.com)
    // +     bugfix by: Benjamin Lupton
    // +     bugfix by: Allan Jensen (http://www.winternet.no)
    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +     bugfix by: Howard Yeend
    // +    revised by: Luke Smith (http://lucassmith.name)
    // +     bugfix by: Diogo Resende
    // *     example 1: number_format(1234.56);
    // *     returns 1: '1,235'
    // *     example 2: number_format(1234.56, 2, ',', ' ');
    // *     returns 2: '1 234,56'
    // *     example 3: number_format(1234.5678, 2, '.', '');
    // *     returns 3: '1234.57'
    // *     example 4: number_format(67, 2, ',', '.');
    // *     returns 4: '67,00'

    var n = number, prec = decimals, dec = dec_point, sep = thousands_sep;
    n = !isFinite(+n) ? 0 : +n;
    prec = !isFinite(+prec) ? 0 : Math.abs(prec);
    sep = sep == undefined ? ',' : sep;

    var s = n.toFixed(prec),
        abs = Math.abs(n).toFixed(prec),
        _, i;

    if (abs > 1000) {
        _ = abs.split(/\D/);
        i = _[0].length % 3 || 3;

        _[0] = s.slice(0, i + (n < 0)) +
            _[0].slice(i).replace(/(\d{3})/g, sep + '$1');

        s = _.join(dec || '.');
    } else {
        s = abs.replace('.', dec_point);
    }

    return s;
}

//Agregar column dinamica a DataTable
function addColumnDataTable(tabla, objColumn) {
    tabla.row.add(objColumn).draw();
}

function dropColumnDataTable(tabla, id) {
    tabla.row('#' + id).remove().draw(false);
}

function tableSearch(obj) {
    var oTable = $('#' + obj.id).DataTable({
        colReorder: true,
        responsive: (obj.hasOwnProperty('responsive') ? obj.serverSide : true),
        processing: true,
        "bAutoWidth": false,
        language: {
            'url': "/vendors/datatables/Spanish.json"
        },
        serverSide: (obj.hasOwnProperty('serverSide') ? obj.serverSide : false),
        searching: (obj.hasOwnProperty('searching') ? obj.searching : true),
        paging: (obj.hasOwnProperty('paging') ? obj.paging : true),
        destroy: (obj.hasOwnProperty('destroy') ? obj.destroy : false),
        order: (obj.hasOwnProperty('sort') ? obj.sort : [[0, 'asc']]),
        ajax: {
            type: obj.type,
            url: obj.url,
            data: function (d) {
                $("#" + obj.form).find(':input').each(function () {
                    var element = this;
                    if (element.type == 'checkbox') {
                        if (element.name != '') {
                            d[element.name] = element.checked;
                        }
                    } else {
                        if (element.name != '' && element.value != '') {
                            d[element.name] = element.value;
                        }
                    }
                });
            }
        },
        columns: obj.columns,
        lengthMenu: ((obj.hasOwnProperty('lengthMenu')) ? obj.lengthMenu : [[50, 100, 1000], [50, 100, 1000]]),
        "createdRow": function (row, data) {
            if (obj.hasOwnProperty('checkInactive')) {
                if (data.state == 'INACTIVO') {
                    $(row).addClass("inactive");
                }
            }
        },
        dom: 'Bfrtip',
        buttons: ((obj.hasOwnProperty('exportButtons')) ? ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5'] : []),
        "footerCallback": function (row, data, start, end, display) {
            if (obj.hasOwnProperty('colTotal')) {
                var api = this.api(), data;

                var intVal = function (i) {
                    return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                };

                $.each(obj.colTotal, function (key, value) {
                    var totalCol = api
                        .column(value[0], {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    var htmlTotal = (value[1] == 't') ? numberFormat(totalCol, 0, ',', '.') : totalCol;
                    $(api.column(value[0]).footer()).html(htmlTotal);
                });
            }
        }
    });

    $("#" + obj.form).on('submit', function (e) {
        oTable.draw();
        e.preventDefault();
    });

    if(obj.hasOwnProperty('showHide') && makeColumns){
        $.each(obj.columns, function (key, value) {
            if(value.hasOwnProperty('column')){
                var check = (value.visible == true) ? '' : 'checked';
                $('.to_do').append('<li><p><input type="checkbox" onclick="hideColumn(this)" data-table="'+obj.id+'" '+check+' class="toggle-vis" data-column="'+key+'">&nbsp;'+value.column+' </p></li>');
            }
        });

        $('[data-column]').click();
        makeColumns = false;
    }

    return oTable;
}

function hideColumn(opt) {
    var tableId = $(opt).data('table');
    var visible = $(opt).prop( "checked" );
    tableId = $('#' + tableId).DataTable();
    tableId.column( $(opt).attr('data-column') ).visible( visible );
}
