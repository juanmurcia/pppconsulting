<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs')
    ->middleware('permission:logs.index');

/** Auditoria **/
Route::get('audit/{pattern}/{model}', 'AuditController@list')->name('audit.view');

/** Admin **/
Route::post('program', 'ProgramController@store')->name('program.store')
    ->middleware('permission:program.create');
Route::put('program/{program}', 'ProgramController@update')->name('program.update')
    ->middleware('permission:program.update');

Route::post('lesson/{program}', 'LessonController@store')->name('lesson.store')
    ->middleware('permission:lesson.create');

Route::post('class/{lesson}', 'ClassesController@store')->name('class.store')
    ->middleware('permission:class.create');

Route::put('lesson/{lesson}', 'LessonController@update')->name('lesson.update')
    ->middleware('permission:lesson.create');

Route::put('class/{class}', 'ClassesController@update')->name('class.update')
    ->middleware('permission:class.update');

Route::get('usuarios', 'UserController@index')->name('user.index')
    ->middleware('permission:user.index');

Route::get('user', 'UserController@list')->name('user.table')
    ->middleware('permission:user.index');

Route::post('user', 'UserController@store')->name('user.store')
    ->middleware('permission:user.store');

Route::get('user/{user}', 'UserController@show')->name('user.update')
    ->middleware('permission:user.update');

Route::put('user/{user}', 'UserController@update')->name('user.update')
    ->middleware('permission:user.update');

Route::get('contract_user/{user}', 'ContractProgramController@list_user')->name('contract_program.user')
    ->middleware('permission:contract_program.user');

Route::post('contract_user/{user}', 'ContractProgramController@store_user')->name('contract_program.store')
    ->middleware('permission:contract_program.store');

Route::delete('contract_user/{contract}', 'ContractProgramController@delete_user')->name('contract_program.delete')
    ->middleware('permission:contract_program.delete');


/** Public **/
Route::get('programs/list', 'ProgramController@list_user')->name('program.list_user');
Route::get('programs/{program}/learn', 'ProgramController@learn')->name('program.learn');
Route::get('programs/{program}/progress/{lesson}/{class}', 'ProgramController@lesson')->name('program.lesson');
Route::get('programs/{program}/progress', 'ProgramController@lesson')->name('program.lesson');
Route::get('programs/{program}', 'ProgramController@show')->name('program.show');

Route::get('lesson/{lesson}/{program}/class', 'LessonController@class')->name('lesson.class');

Route::get('classes/{class}', 'ClassesController@show')->name('classes.show');
Route::get('classes/{class}/{program}/completed', 'ClassesController@completed')->name('classes.completed');

Route::get('profile', 'UserController@view')->name('user.view');
Route::put('user_profile', 'UserController@profile')->name('user.profile');
Route::post('user_password', 'UserController@changePassword')->name('user.password');


/** Paypal**/
// route for processing payment
Route::get('program/{program}/paypal', 'PaymentController@payWithpaypal');
Route::post('program/{program}/pay', 'PaymentController@savePaymentWithCard');
// route for check status of the payment
Route::get('status', 'PaymentController@getPaymentStatus');
Route::get('program_payed', 'ContractProgramController@finishPayment');


/** Guest **/
// E-mail verification
Route::get('account', 'GuestController@register');
Route::post('register', 'GuestController@store');
Route::get('confirmed_register', 'GuestController@confirmed');
Route::get('register/verify/{code}', 'GuestController@verify');