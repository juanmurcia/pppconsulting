<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdvanceProgram;
use App\Models\ContractProgram;
use App\Models\Classes;
use App\models\TranslatorDetail;

class ClassesController extends Controller
{
    public function store(Request $request, $lesson_id)
    {
        if ($request->ajax())
        {
            $order = Classes::where("lesson_id",$lesson_id)->max('order');
            $class = new Classes([
                'name' => 'Nueva',
                'description' => 'Agregar Descripción de la clase',
                'info_video' => '302344978',
                'order' => ($order + 1),
                'state_id' => 40,
                'lesson_id' => $lesson_id,
                'user_cre_id' => auth()->user()->id,
            ]);

            $class->save();
            return response()->json(['success' => true, 'data', $class]);
        }
    }

    public function update(Request $request, $id)
    {
        if ($request->ajax())
        {
            $data = [
                'name' => $request['name'],
                'description' => $request['description'],
                'info_video' => $request['class_video'],
                'user_mod_id' => auth()->user()->id,
            ];

            Classes::find($id)->update($data);

            return response()->json(['success' => true, 'id' => $id]);
        }
    }

    public function show($id, Request $request)
    {
        if($request->ajax()){
            $arr_class = Classes::where('id',$id)->get();
            $data = [];

            if(true or Auth()->user()->lang == 'es'){
                $data = $arr_class;
            }else{
                foreach ($arr_class as $key => $info){
                    $translator = TranslatorDetail::select(['field','text'])
                        ->where('pattern_id',Classes::$pattern_id)
                        ->where('lang',Auth()->user()->lang)
                        ->where('model_id',$info->id)
                        ->whereIn('field',['name','description'])
                        ->get();

                    foreach($translator as $value){
                        $data[$key][$value->field] = $value->text;
                    }

                    $data[$key]['id'] = $info->id;
                    $data[$key]['state_id'] = $info->state_id;
                    $data[$key]['order'] = $info->order;
                    $data[$key]['info_video'] = $info->info_video;
                }
            }

            return response()->json($data[0]);
        }
    }

    public function completed($id, $program_id, Request $request){
        if($request->ajax()){
            $contract_program = ContractProgram::where('program_id',$program_id)
                ->where('state_id',90)
                ->where('user_id',auth()->user()->id)
                ->get();

            if(!$contract_program[0]){
                return redirect('/');
            }

            $advance_program = AdvanceProgram::where('contract_program_id', $contract_program[0]->id)
                ->where('class_id',$id)
                ->get();

            AdvanceProgram::find($advance_program[0]->id)->update(['completed' => true]);

            return response()->json(['success' => true]);
        }
    }
}
