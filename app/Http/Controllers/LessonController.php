<?php

namespace App\Http\Controllers;

use App\Models\Program;
use Illuminate\Http\Request;
use App\Models\AdvanceProgram;
use App\Models\ContractProgram;
use App\Models\Classes;
use App\Models\Lesson;
use App\models\TranslatorDetail;

class LessonController extends Controller
{
    public function store(Request $request, $program_id)
    {
        if ($request->ajax())
        {
            $order = Lesson::where("program_id",$program_id)->max('order');
            $lesson = new Lesson([
                'name' => 'Nueva',
                'order' => ($order + 1),
                'program_id' => $program_id,
                'state_id' => 30,
                'user_cre_id' => auth()->user()->id,
            ]);

            $lesson->save();
            return response()->json(['success' => true, 'data', $lesson]);
        }
    }

    public function update(Request $request, $id)
    {
        if ($request->ajax())
        {
            $data = [
                'name' => $request['lesson_name'],
                'user_mod_id' => auth()->user()->id,
            ];

            Lesson::find($id)->update($data);

            return response()->json(['success' => true, 'id' => $id]);
        }
    }

    public function class($id, $program_id, Request $request)
    {
        if($request->ajax()){
            $class = Classes::where('lesson_id',$id)
                ->where('state_id',40)
                ->orderBy('order')
                ->get();

            $data = [];
            $arr_class = [];

            foreach ($class as $key => $val){
                $arr_class[$key] = $val;
                $contract_program = ContractProgram::where('program_id',$program_id)
                    ->where('state_id',90)
                    ->where('user_id',auth()->user()->id)
                    ->first();

                if(auth()->user()->roles()->whereIn('roles.slug',['admin'])->doesntExist()){
                    if(is_null($contract_program)){
                        return response()->json([]);
                    }


                    $advance_program = AdvanceProgram::select(['completed'])
                        ->where('contract_program_id',$contract_program->id)
                        ->where('class_id',$val->id)
                        ->first();

                    $completed = is_null($advance_program) ? false : $advance_program->completed;

                    $arr_class[$key]['completed'] = $completed;
                }
            }
            $data = $arr_class;
            /*
            if(true AND Auth()->user()->lang == 'es'){
                $data = $arr_class;
            }else{
                foreach ($arr_class as $key => $info){
                    $translator = TranslatorDetail::select(['field','text'])
                        ->where('pattern_id',Classes::$pattern_id)
                        ->where('lang',Auth()->user()->lang)
                        ->where('model_id',$info->id)
                        ->whereIn('field',['name','description'])
                        ->get();

                    foreach($translator as $value){
                        $data[$key][$value->field] = $value->text;
                    }

                    $data[$key]['id'] = $info->id;
                    $data[$key]['completed'] = $info->completed;
                    $data[$key]['order'] = $info->order;
                }
            }*/
            return response()->json($data);
        }
    }
}
