<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContractProgram;

class ContractProgramController extends Controller
{
    public function store_user(Request $request, $user_id)
    {
        if ($request->ajax())
        {
            $count = ContractProgram::where("program_id",$request['optProgram'])
                ->where("user_id",$user_id)
                ->where("state_id",90)
                ->count();

            if($count > 0){
                return response()->json(['success' => false, 'msj'=>"Ya existe contrato para el usuario y programa"]);
            }

            $contract_program = new ContractProgram([
                'program_id' => $request['optProgram'],
                'user_id' => $user_id,
                'license_id' => 1,
                'state_id' => 90,
                'user_cre_id' => auth()->user()->id,
            ]);

            $contract_program->save();
            return response()->json(['success' => true]);
        }
    }

    public function delete_user(Request $request, $id)
    {
        if ($request->ajax())
        {
            $data = [
                'state_id' => 91,
                'user_mod_id' => auth()->user()->id,
            ];

            ContractProgram::find($id)->update($data);

            return response()->json(['success' => true]);
        }
    }

    function list_user(Request $request, $user_id){
        $contract_programs = [];
        if ($request->ajax()) {
            $contract_programs = ContractProgram::with(['program'])->where("user_id", $user_id)->where("state_id", 90)->get();
        }
        return response()->json(['data' => $contract_programs]);
    }

    public function finishPayment(){
        return view('program.payed');
    }
}
