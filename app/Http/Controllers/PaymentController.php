<?php
namespace App\Http\Controllers;

use App\Models\Billing;
use App\Models\Program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\Details;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;

class PaymentController extends Controller
{
    public $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

    }
    public function index()
    {
        return view('paywithpaypal');
    }

    public function savePaymentWithCard(Request $request, $program_id) {

        $program = Program::find($program_id);
        $billing_number = Billing::count();

        $billing = new Billing([
            'number' => $billing_number+1,
            'cost' => $program->cost,
            'state_id' => 50,
            'program_id' => $program_id,
            'user_id' => auth()->user()->id,
        ]);

        $billing->save();

        $card = new CreditCard();
        $card->setType($request['cardType'])
            ->setNumber($request['number'])
            ->setExpireMonth($request['expiryM'])
            ->setExpireYear("20".$request['expiryY'])
            ->setCvv2($request['secure'])
            ->setFirstName($request['name'])
            ->setLastName($request['last_name']);

        $fi = new FundingInstrument();
        $fi->setCreditCard($card);

        $payer = new Payer();
        $payer->setPaymentMethod("credit_card")
            ->setFundingInstruments(array($fi));

        $item1 = new Item();
        $item1->setName($program->name)
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku("1")
            ->setPrice($billing->cost);

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

        $details = new Details();
        $details->setShipping(0)->setTax(0)->setSubtotal($billing->cost);

        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($billing->cost)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Curso ".$program->landing_name)
            ->setInvoiceNumber(uniqid());

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);

            if($payment->transactions[0]->amount->currency != 'USD'){
                return response()->json(['success'=>false, 'msj' => "No coincide la moneda de la transacción"]);
            }

            if($payment->transactions[0]->amount->total != $program->cost){
                return response()->json(['success'=>false, 'msj' => "No coincide el valor de la transqacción con la compra"]);
            }

            if($payment->state != 'approved'){
                return response()->json(['success'=>false, 'msj' => "Transacción rechazada"]);
            }

            $this->contractPayment($billing->id, $payment);
            return response()->json(['success'=>true, 'data' => $billing->id]);

        } catch (PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData();
            exit;
        }
    }

    public function payWithpaypal($program_id)
    {
        $program = Program::find($program_id);
        $billing_number = Billing::count();

        $billing = new Billing([
            'number' => $billing_number+1,
            'cost' => $program->cost,
            'state_id' => 50,
            'program_id' => $program_id,
            'user_id' => auth()->user()->id,
        ]);

        $billing->save();

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();

        $item_1->setName('Item 1')/** item name **/
        ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($billing->cost);
        /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($billing->cost);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription($program->name);

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('status'))/** Specify return URL **/
            ->setCancelUrl(URL::to('status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/

        try {
            $payment->create($this->_api_context);

            if($payment->transactions[0]->amount->currency != 'USD'){
                \Session::put('error', 'No coincide la moneda de la transacción');
                return Redirect::to("/programs/$program_id/learn");
            }

            if($payment->transactions[0]->amount->total != $program->cost){
                \Session::put('error', 'No coincide el valor de la transqacción con la compra');
                return Redirect::to("/programs/$program_id/learn");
            }

            if($payment->state != 'approved'){
                \Session::put('error', 'Transacción rechazada');
                return Redirect::to("/programs/$program_id/learn");
            }

            $this->contractPayment($billing->id, $payment);
            return Redirect::to("/program_payed");

        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::to("/programs/$program_id/learn");
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to("/programs/$program_id/learn");
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());

        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);

        }

        \Session::put('error', 'Unknown error occurred');
        return Redirect::to('/');
    }

    public function contractPayment($billing_id, $payment){
        Billing::find($billing_id)->update([
            'transaction_id' => $payment->id,
            'transaction_state' => $payment->state,
            'state_id' => 51,
        ]);

        return true;
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::to('/');
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        if ($result->getState() == 'approved') {
            \Session::put('success', 'Payment success');
            return Redirect::to('/');
        }

        \Session::put('error', 'Payment failed');
        return Redirect::to('/');
    }
}