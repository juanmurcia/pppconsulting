<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Mail\VerifyEmail;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class GuestController extends Controller
{
    public function register(){
       return view("auth.register");
    }

    public function confirmed(){
       return view("user.confirmed");
    }

    public function store(UserCreateRequest $request)
    {
        if ($request->ajax())
        {
            $request['confirmation_code'] = str_random(25);

            $user = new User([
                'name' => $request['name'],
                'last_name' => $request['last_name'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'lang' => "es",
                'password' => bcrypt($request['password']),
                'confirmation_code' => $request['confirmation_code'],
                'state_id' => 10,
            ]);
            $user->save();
            $user->assignRole(2);

            $data = $request->all();

            // Send confirmation code
            Mail::to($data['email'])->send(new VerifyEmail($data));
            return response()->json(['success' => true]);
        }
    }

    public function verify($code)
    {
        $user = User::where('confirmation_code', $code)->first();

        if (! $user){
            \Session::put('error', 'Error de autenticación');
            return redirect('/');
        }

        $user->confirmed = true;
        $user->confirmation_code = null;
        $user->save();

        \Session::put('error', 'Has confirmado correctamente tu correo');
        return redirect('/');
    }
}
