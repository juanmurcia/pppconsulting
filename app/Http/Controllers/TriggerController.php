<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TriggerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('trigger.create');
    }

    public function autoModel(){
        $result = DB::select("SELECT TABLE_NAME AS model FROM information_schema.COLUMNS 
        WHERE TABLE_SCHEMA = :db AND COLUMN_NAME IN ('user_mod_id') ORDER BY 1 DESC;",['db'=> env('DB_DATABASE')]);

        return $result;
    }


    public function create(Request $request)
    {
        if ($request->ajax())
        {
            if(empty($request['txtModel'])){
                return response()->json(['ErrorBd' => 'Ingrese el modelo']);
            }

            $sql = $this->structureTrigger($request['txtModel']);
            $sql .= $this->structureField($request['txtModel']);
            $sql .= ' 
            END; ';

            return response()->json(['mensaje' => 'Done', 'sql' => $sql]);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function make($model, $structure)
    {
        $sql = '';
        if($structure){
            $sql .= $this->structureTrigger($model);
        }

        $sql .= $this->structureField($model);

        if($structure){
            $sql .= ' 
            END; ';
        }

        return $sql;
    }


    public function structureTrigger($model)
    {
        $sql = '
        DROP PROCEDURE IF EXISTS `'.$model.'_after_update`;
        CREATE TRIGGER '.$model.'_after_update AFTER UPDATE ON '.$model.' FOR EACH ROW
            BEGIN                    
        ';
        return $sql;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function structureField($model)
    {
        $data = ['model'=> $model,'db'=> env('DB_DATABASE')];

        $result = DB::select("SELECT COLUMN_NAME AS field FROM information_schema.COLUMNS 
        WHERE TABLE_NAME = :model AND TABLE_SCHEMA = :db AND 
        COLUMN_NAME NOT IN ('id','user_cre_id', 'user_mod_id', 'created_at', 'updated_at');",$data);

        $sql = " INSERT INTO audit_$model (";
        $value = "";

        if(sizeof($result) > 0){
            foreach($result as $field ){
                $sql .= "$field->field,";
                $value .= " CASE WHEN OLD.$field->field <> NEW.$field->field THEN NEW.$field->field ELSE NULL END, ";
            }

            $sql.=" id, user_cre_id, created_at) SELECT $value NEW.id, NEW.user_mod_id, CURRENT_TIMESTAMP;";
        }

        return $sql;
    }
}
