<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lesson;
use App\Models\Program;
use App\Models\TranslatorDetail;
use App\Models\ContractProgram;
use App\Http\Requests\programCreateRequest;
use App\Http\Requests\programUpdateRequest;
use DataTables;

class ProgramController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function store(programCreateRequest $request)
    {
        if ($request->ajax())
        {
            $program = new Program([
                'landing_name' => $request['name'],
                'landing_description' => $request['description'],
                'state_id' => $request['state'],
                'lang' => $request['lang'],
                'user_cre_id' => auth()->user()->id,
            ]);

            $program->save();

            return response()->json(['success' => true, 'id', $program->id]);
        }
    }

    public function update(programUpdateRequest $request, $id)
    {
        if ($request->ajax())
        {
            $data = [
                'name' => $request['name'],
                'description' => $request['description'],
                'landing_name' => $request['landing_name'],
                'landing_description' => $request['landing_description'],
                'landing_video' => $request['landing_video'],
                'cost' => $request['cost'],
                'state_id' => $request['state'],
                'lang' => $request['lang'],
                'user_mod_id' => auth()->user()->id,
            ];

            Program::find($id)->update($data);

            return response()->json(['success' => true, 'id', $id]);
        }
    }

    public function learn($id)
    {
        return view('program.learn', compact('id'));
    }

    public function lesson($id, $lesson_id = 0, $class_id = 0)
    {
        return view('program.lesson', compact('id', 'lesson_id','class_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        if($request->ajax()){
            $program = Program::with(['lessons'=> function ($query) {
                $query->where('state_id', 30);
            }])->where('id',$id)->select('programs.*')->get();
            $data = [];
            $arr_program = [];

            if(!isset($program[0])){
                return response()->json([]);
            }

            foreach ($program as $key => $val){
                $arr_program[$key] = $val;
                $contract_program = ContractProgram::where('user_id',auth()->user()->id)
                    ->where('program_id',$val->id)
                    ->get();

                if(sizeof($contract_program)){
                    $arr_program[$key]['state_id'] = 13;
                    $arr_program[$key]['advance'] = $contract_program[0]['advance'];
                }
            }


            foreach ($arr_program as $key => $info){
                $data[$key] = $info;
                if($info->multi_lang) {
                    $translator = TranslatorDetail::select(['field', 'text'])
                        ->where('pattern_id', Program::$pattern_id)
                        ->where('lang', Auth()->user()->lang)
                        ->where('model_id', $info->id)
                        ->whereIn('field', ['name', 'description', 'landing_name', 'landing_description'])
                        ->get();

                    foreach ($translator as $value) {
                        $data[$key][$value->field] = $value->text;
                    }

                    foreach ($info['lessons'] as $ind => $lesson) {
                        $data[$key]['lessons'][$ind] = $lesson;
                        $translator = TranslatorDetail::select(['text'])
                            ->where('pattern_id', Lesson::$pattern_id)
                            ->where('lang', Auth()->user()->lang)
                            ->where('model_id', $lesson->id)
                            ->whereIn('field', ['name'])
                            ->get();

                        $data[$key]['lessons'][$ind]['name'] = $translator[0]->text;
                    }
                }

            }

            return response()->json($data[0]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_user(Request $request)
    {
        if($request->ajax()){
            $programs = Program::select(['id','landing_name','landing_description','landing_image','state_id','lang','multi_lang'])
                ->whereIn('state_id',[20,21])
                ->get();

            $data = [];
            $arr_program = [];

            foreach ($programs as $key => $program){
                $arr_program[$key] = $program;
                $contract_program = ContractProgram::where('user_id',auth()->user()->id)
                    ->where('state_id',90)
                    ->where('program_id',$program->id)
                    ->get();

                if(sizeof($contract_program)){
                    $arr_program[$key]['state_id'] = 23;
                    $arr_program[$key]->advance = $contract_program[0]['advance'];
                }
            }

            foreach ($arr_program as $key => $info){
                $data[$key] = $info;
                if($info->multi_lang){
                    $translator = TranslatorDetail::select(['field','text'])
                        ->where('pattern_id',Program::$pattern_id)
                        ->where('lang',Auth()->user()->lang)
                        ->where('model_id',$info->id)
                        ->whereIn('field',['landing_name','landing_description'])
                        ->get();

                    foreach($translator as $value){
                        $data[$key][$value->field] = $value->text;
                    }

                    $data[$key]['id'] = $info->id;
                    $data[$key]['state_id'] = $info->state_id;
                    $data[$key]['advance'] = $info->advance;
                }
            }

            return response()->json($data);
        }
    }
}
