<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\Pattern;

class AuditController extends Controller
{
    public $arr_name;

    function list($pattern_id, $model_id){
        $pattern = Pattern::find($pattern_id);
        $role = auth()->user();
        $permission = $pattern->model.'.audit';

        if(!$role->can($permission)){
            return response()->json(['ErrorBd' => 'No posee permisos de auditoría de '.$pattern->name]);
        }

        if(sizeof($pattern) == 0){
            return response()->json(['ErrorBd' => 'Modelo no encontrado o configurado']);
        }

        $pattern = str_replace(' ','','App\Models\ '.$pattern->model);
        $model = $pattern::where('id',$model_id)->first();
        $this->arr_name = $model->audit_field;

        if(sizeof($model) == 0){
            return response()->json(['ErrorBd' => 'Registro no encontrado']);
        }

        $arr_audit =
            DB::table('audit_'.$model->table.' AS A')
                ->select('A.*', 'users.name AS username')
                ->join('users','A.user_cre_id','=','users.id')
                ->where('A.id',$model_id)
                ->orderBy('A.created_at','desc')
                ->get();

        $audit = [];

        foreach ($arr_audit as $item) {
            $obj = $this->validate_field($item);
            $obj['created_at'] = $item->created_at;
            $obj['user'] = $item->username;
            array_push($audit, $obj);
        }
        return response()->json(['data' => $audit]);
    }

    function validate_field($item){
        $new_item = [];
        foreach ($item as $key => $item) {
            $new_item[$key] = (is_null($item) ? '' : $this->field_value($key, $item));
        }

        return $new_item;
    }

    function field_value($key, $value){
        $field = $value;

        if(isset($this->arr_name[$key]['model'])){
            $pattern = str_replace(' ', '', 'App\Models\ '.$this->arr_name[$key]['model']);
            $model = $pattern::where('id',$value)->first();
            //$new = (in_array($this->arr_name[$item->field]['model'],['Company','Professional']) ? $model->person->name.' '.$model->person->last_name : $model->name);
            $field = $model->name;
        }

        return [$field];
    }
}
