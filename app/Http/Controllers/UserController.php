<?php

namespace App\Http\Controllers;

use App\Http\Requests\userPasswordRequest;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use DataTables;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.table');
    }

    /**
     * Show the dataTable.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $role = auth()->user();
            $users = User::with(['roles'])->select('users.*');

            return Datatables::eloquent($users)
                ->addColumn('state', function ($user) use ($role) {
                    return ($user->state_id == 10 ? 'ACTIVO' : 'INACTIVO');
                })
                ->addColumn('role', function ($user) use ($role) {
                    $role=$user->roles()->first();
                    if(is_null($role)){
                        return '';
                    }else{
                        return $role->name;
                    }
                })
                ->addColumn('buttons', function ($user) use ($role) {
                    $button = '';

                    if ($role->can('user.edit')) {
                        $button .= '<a href="javascript:void(0)" class="btn btn-sm btn-default btn-icon btn-outline text-center" onclick="editUser(' . $user->id . ')" title="Editar | Edit" rol="tooltip" data-toggle="modal" data-target="#modUser"><i class="fa fa-pencil text-center" aria-hidden="true"></i></a>';
                        $button .= '<a href="javascript:void(0)" onclick="contract_user('. $user->id . ');" class="btn btn-sm btn-warning btn-icon btn-outline text-center" title="Ver contratos" rol="tooltip" data-toggle="modal" data-target="#modContract"><i class="fa fa-book text-center" aria-hidden="true"></i></a>';
                    }

                    if ($role->can('user.destroy')) {
                        $button .= ($user->state_id == 10 ?
                            '<a href="javascript:void(0)" class="btn-inactivate btn btn-sm btn-danger btn-icon btn-outline text-center" data-message="'.trans_choice("text.inactivate",2).trans_choice("text.user",1).'?" onclick="destroyUser(' . $user->id . ')" title="'.trans_choice("text.inactivate",1).'" rol="tooltip" ><i class="fa fa-close text-center" aria-hidden="true"></i></a>'
                            :
                            '<a href="javascript:void(0)" class="btn-activate btn btn-sm btn-success btn-icon btn-outline text-center" data-message="'.trans_choice("text.activate",2).trans_choice("text.user",1).'?" onclick="activateUser(' . $user->id . ')" title="'.trans_choice("text.activate",1).'" rol="tooltip" ><i class="fa fa-check text-center" aria-hidden="true"></i></a>'
                        );
                    }
                    $button .= '';
                    return $button;
                })
                ->rawColumns(['buttons'])
                ->setRowId('id')
                ->make(true);
        }
    }

    public function store(UserCreateRequest $request)
    {
        if ($request->ajax())
        {
            $user = new User([
                'name' => $request['name'],
                'last_name' => $request['last_name'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'lang' => "es",
                'password' => bcrypt($request['password']),
                'state_id' => 10,
                'role_id' => 2,
                'user_cre_id' => auth()->user()->id,
            ]);
            $user->save();
            $user->assignRole(2);

            return response()->json(['message' => 'success']);
        }
    }

    public function view()
    {
        return view('user.view');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with(['roles'])->where('id',$id)->get();
        $user[0]->role_id = $user[0]->roles[0]->slug;
        return response()->json($user[0]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        User::find($id)->update([
            'state_id' => 5,
            'user_mod_id' => auth()->user()->id,
        ]);

        return response()->json(['message' => 'success']);
    }


    public function update(UserUpdateRequest $request, $id)
    {
        if ($request->ajax())
        {
            $user = User::find($id);
            $user->update([
                'document' => $request['document'],
                'name' => $request['name'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'user_mod_id' => auth()->user()->id,
            ]);
            $user->revokeAllRoles();
            $user->assignRole($request['role_id']);

            return response()->json(['message' => 'success']);
        }
    }

    public function profile(UserUpdateRequest $request)
    {
        if ($request->ajax())
        {
            $user = User::find(auth()->user()->id);
            $user->update([
                'name' => $request['name'],
                'last_name' => $request['last_name'],
                'email' => $request['email'],
                'address' => $request['address'],
                'phone' => $request['phone'],
                'lang' => $request['lang'],
                'user_mod_id' => auth()->user()->id,
            ]);
            $user->revokeAllRoles();
            $user->assignRole(2);

            return response()->json(['success' => true]);
        }
    }

    public function changePassword(userPasswordRequest $request)
    {
        if ($request->ajax()) {
            $data = [
                'password' => bcrypt($request['txtPassUserConf']),
                'user_mod_id' => auth()->user()->id,
            ];
            user::find(auth()->user()->id)->update($data);

            return response()->json(['mensaje' => 'Modificado', 'success' => true]);
        }
    }

    public function destroy($id)
    {
        User::find($id)->update([
            'state_id' => 6,
            'user_mod_id' => auth()->user()->id,
        ]);

        return response()->json(['message' => 'success']);
    }
}
