<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules= User::$rules;
        $rules['password'] = 'required|min:6';
        $rules['confirm'] = 'required|same:password';

        return $rules;
    }
    /** Get alias from data form
     * @return array
     */

    public function attributes()
    {
        return User::$fields;
    }
}
