<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractProgram extends Model
{
    public $table = 'contract_programs';
    public static $pattern_id = 9;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'program_id',
        'user_id',
        'license_id',
        'state_id',
        'advance',
    ];

    public $audit_field = [
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "program_id" => "integer",
        "user_id" => "integer",
        "advance" => "integer",
    ];

    public function program()
    {
        return $this->belongsTo('App\Models\Program', 'program_id');
    }
}
