<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    public $table = 'billings';
    public static $pattern_id = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number',
        'state_id',
        'user_id',
        'program_id',
        'cost',
        'transaction_id',
        'transaction_state',
    ];
}
