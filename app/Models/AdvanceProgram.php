<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvanceProgram extends Model
{
    public $table = 'advance_programs';
    public static $pattern_id = 10;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contract_program_id',
        'class_id',
        'completed',
    ];

    public $audit_field = [

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "contract_program_id" => "integer",
        "class_id" => "integer",
    ];

    public static $rules = [
        'contract_program_id' => 'required',
        'class_id' => 'required',
        'completed' => 'required',
    ];
}
