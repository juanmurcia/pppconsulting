<?php

namespace App\Models;

use Caffeinated\Shinobi\Traits\ShinobiTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, ShinobiTrait;
    public $table = 'users';
    public static $pattern_id = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'phone',
        'address',
        'password',
        'state_id',
        'confirmation_code',
        'confirmed',
        'lang',
        'user_cre_id',
        'user_mod_id'
    ];

    public $audit_field = [
        "state_id" => ['model'=>'State'],
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "state_id" => "integer",
        "name" => "string|Min:3",
        "email" => "string|email",
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $rules = [
        'name' => 'required',
        'email' => 'required'
    ];

    public static $rules_password = [
        'txtPassPrev' => 'required|min:3|current_password',
        'txtPassUserNew' => 'required|min:6',
        'txtPassUserConf' => 'required|same:txtPassUserNew'
    ];


    public static $fields = [
        'name' => 'Nombre',
    ];

    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\Models\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\Models\User', 'user_mod_id');
    }

    public function roles()
    {
        return $this->belongsToMany('Caffeinated\Shinobi\Models\Role');
    }
}
