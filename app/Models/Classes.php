<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    public $table = 'classes';
    public static $pattern_id = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'info_video',
        'order',
        'lesson_id',
        'state_id',
        'user_cre_id',
        'user_mod_id'
    ];

    public $audit_field = [
        "state_id" => ['model'=>'State'],
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "state_id" => "integer",
        "name" => "string|Min:3",
    ];

    public static $rules = [
        'name' => 'required',
    ];


    public static $fields = [
        'name' => 'Nombre',
    ];

    public function lesson()
    {
        return $this->belongsTo('App\Models\Lesson', 'lesson_id');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\Models\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\Models\User', 'user_mod_id');
    }
}
