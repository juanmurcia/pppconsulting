<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    public $table = 'programs';
    public static $pattern_id = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'landing_name',
        'landing_description',
        'landing_image',
        'landing_video',
        'state_id',
        'rate',
        'cost',
        'lang',
        'multi_lang',
        'user_cre_id',
        'user_mod_id'
    ];

    public $audit_field = [
        "state_id" => ['model'=>'state'],
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "user_cre_id" => "integer",
        "user_mod_id" => "integer",
        "state_id" => "integer",
        "name" => "string|Min:3",
    ];

    public static $rules = [
        'name' => 'required',
        'description' => 'required',
        'state' => 'required',
        'lang' => 'required',
    ];

    public static $fields = [
        'name' => 'Nombre',
        'description' => 'Descripcion',
        'state' => 'state',
        'lang' => 'language',
    ];

    public function lessons()
    {
        return $this->hasMany('App\Models\Lesson', 'program_id');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\state', 'state_id');
    }

    public function user_cre()
    {
        return $this->belongsTo('App\Models\User', 'user_cre_id');
    }

    public function user_mod()
    {
        return $this->belongsTo('App\Models\User', 'user_mod_id');
    }
}
