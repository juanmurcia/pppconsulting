<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class TranslatorDetail extends Model
{
    public $table = 'translator_details';
    public static $pattern_id = 8;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pattern_id',
        'model_id',
        'field',
        'text',
        'lang',
    ];

    public $audit_field = [
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "pattern_id" => "integer",
        "model_id" => "integer",
        "field" => "string|Min:3",
    ];
}
